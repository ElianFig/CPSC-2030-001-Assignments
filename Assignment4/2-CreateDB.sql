/*
CREATE DATABASE
*/
CREATE DATABASE pokedex;

/*
SET DATABASE AS DEFAULT
*/
USE pokedex;

/*
CREATE TABLES STATEMENTS
*/
CREATE TABLE pokemon (
    nat_ID INTEGER,
    hoenn_ID INTEGER,
    name CHAR(50) NOT NULL,
    type CHAR(50) NOT NULL,
    HP INTEGER,
    ATK INTEGER,
    DEF INTEGER,
    SAT INTEGER,
    SDF INTEGER,
    SPD INTEGER,
    BST INTEGER,
    type1 CHAR(50) NOT NULL,
    type2 CHAR(50),
    type1_ID INTEGER NOT NULL,
    type2_ID INTEGER,
    img_url CHAR(255),
    favorite BOOLEAN,
    PRIMARY KEY (nat_ID)
);

CREATE TABLE poke_form (
    form_ID INTEGER,
    nat_ID INTEGER,
    hoenn_ID INTEGER,
    name CHAR(50) NOT NULL,
    type CHAR(50) NOT NULL,
    HP INTEGER,
    ATK INTEGER,
    DEF INTEGER,
    SAT INTEGER,
    SDF INTEGER,
    SPD INTEGER,
    BST INTEGER,
    type1 CHAR(50) NOT NULL,
    type2 CHAR(50),
    type1_ID INTEGER NOT NULL,
    type2_ID INTEGER,
    img_url CHAR(255),
    favorite BOOLEAN,
    PRIMARY KEY (form_ID),
    FOREIGN KEY (nat_ID) REFERENCES pokemon(nat_ID)
);

CREATE TABLE poke_pop (
    nat_ID INTEGER,
    name CHAR(50) NOT NULL,
    link CHAR(255),
    PRIMARY KEY (nat_ID)
);

CREATE TABLE type (
    type_ID INTEGER,
    type CHAR(50) NOT NULL,
    strong CHAR(255),
    weak CHAR(255),
    resistant CHAR(255),
    vulnerable CHAR(255),
    PRIMARY KEY (type_ID)
);

CREATE TABLE poke_type (
    poke_ID INTEGER,
    type_ID INTEGER,
    PRIMARY KEY (poke_ID, type_ID),
    FOREIGN KEY (poke_ID) REFERENCES pokemon(nat_ID),
    FOREIGN KEY (type_ID) REFERENCES type(type_ID)

);

/* TABLE STRONG AGAINST - SELF-REFERENCE */
CREATE TABLE strong (
    type_ID INTEGER,
    strong_ID INTEGER,
    PRIMARY KEY (type_ID, strong_ID),
    FOREIGN KEY (type_ID) REFERENCES type(type_ID),
    FOREIGN KEY (strong_ID) REFERENCES type(type_ID)
);

/* TABLE WEAK AGAINST - SELF-REFERENCE */
CREATE TABLE weak (
    type_ID INTEGER,
    weak_ID INTEGER,
    PRIMARY KEY (type_ID, weak_ID),
    FOREIGN KEY (type_ID) REFERENCES type(type_ID),
    FOREIGN KEY (weak_ID) REFERENCES type(type_ID)
);

/* TABLE RESISTANT TO - SELF-REFERENCE */
CREATE TABLE resistant (
    type_ID INTEGER,
    resistant_ID INTEGER,
    PRIMARY KEY (type_ID, resistant_ID),
    FOREIGN KEY (type_ID) REFERENCES type(type_ID),
    FOREIGN KEY (resistant_ID) REFERENCES type(type_ID)
);

/* TABLE VULNERABLE TO - SELF-REFERENCE */
CREATE TABLE vulnerable (
    type_ID INTEGER,
    vulnerable_ID INTEGER,
    PRIMARY KEY (type_ID, vulnerable_ID),
    FOREIGN KEY (type_ID) REFERENCES type(type_ID),
    FOREIGN KEY (vulnerable_ID) REFERENCES type(type_ID)
);


/* CREATE VIEW
I have separated the different forms of pokemon due to the primary key constraint on the table.
Since the original pokemon and all it's forms have the same national ID, I decided to separate it in order to be able to use national ID as a primary key.
That being said, I still need to count the different forms on my queries, and to do that, I've created a view 'pokemon_form_all':
Please execute the command below BEFORE executing any of the SELECT queries. Thanks
*/
CREATE VIEW pokemon_form_all AS
    SELECT nat_ID, name, type, type1, type2, HP, ATK, DEF, SAT, SDF, SPD, BST, img_url, favorite
    FROM pokemon
    UNION
    SELECT nat_ID, name, type, type1, type2, HP, ATK, DEF, SAT, SDF, SPD, BST, img_url, favorite
    FROM poke_form;

/*
CREATE USER TO BE USED LATER IN Assignment 5
*/
CREATE USER IF NOT EXISTS 'CPSC2030'@'localhost' IDENTIFIED VIA mysql_native_password USING '*F29CF4A1DE8DE0164B8FC2F1BE128965912C97EF';
GRANT ALL PRIVILEGES ON *.* TO 'CPSC2030'@'localhost' REQUIRE NONE WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;

/*
CREATE PROCEDURES TO BE USED IN Assignment 5
*/
/*function to get all pokemons*/
DELIMITER //
CREATE PROCEDURE get_pokemon()
BEGIN
	SELECT nat_ID, name, type, type1, type2, img_url, HP, ATK, DEF, SAT, SDF, SPD, BST, favorite
	FROM pokemon_form_all;
END //
DELIMITER ;

/*function to get pokemon with parameter*/
DELIMITER //
CREATE PROCEDURE pokemon_by_type
(IN str text)
BEGIN
SELECT nat_ID, name, type1, type2, img_url
FROM pokemon_form_all
WHERE type1 = str
OR type2 = str;
END //
DELIMITER ;

/*function to get pokemon details*/
DELIMITER //
CREATE PROCEDURE pokemon_info
(IN str INT)
BEGIN
SELECT nat_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, img_url
FROM pokemon
WHERE nat_ID = str;
END //
DELIMITER ;

/*function to get pokemon type weakness and resistance*/
DELIMITER //
CREATE PROCEDURE pokemon_type_stat
(IN str INT)
BEGIN
SELECT DISTINCT t.type, t.strong, t.weak, t.resistant, t.vulnerable
FROM ((pokemon_form_all p
       INNER JOIN poke_type pt ON p.nat_ID = pt.poke_ID)
				  INNER JOIN type t ON pt.type_ID = t.type_ID)
WHERE p.nat_ID = str;
END //
DELIMITER ;

/*function to get pokemon forms*/
DELIMITER //
CREATE PROCEDURE pokemon_form
(IN str INT)
BEGIN
SELECT pf.nat_ID, pf.name, pf.type, pf.img_url
FROM pokemon p
     INNER JOIN poke_form pf ON p.nat_ID = pf.nat_ID
WHERE p.nat_ID = str;
END //
DELIMITER ;


/*function to set favorite pokemons*/
DELIMITER //
CREATE PROCEDURE set_favorite
(IN id INT, IN str text)
BEGIN
UPDATE pokemon
SET favorite = TRUE
WHERE nat_ID = id
AND name = str;
UPDATE poke_form
SET favorite = TRUE
WHERE nat_ID = id
AND name = str;
END //
DELIMITER ;

/*function to remove favorite pokemons*/
DELIMITER //
CREATE PROCEDURE del_favorite
(IN id INT, IN str text)
BEGIN
UPDATE pokemon
SET favorite = FALSE
WHERE nat_ID = id
AND name = str;
UPDATE poke_form
SET favorite = FALSE
WHERE nat_ID = id
AND name = str;
END //
DELIMITER ;

/*function to get favorites pokemons*/
DELIMITER //
CREATE PROCEDURE get_favorites()
BEGIN
	SELECT nat_ID, name, type1, type2, img_url
	FROM pokemon_form_all
  WHERE favorite = TRUE;
END //
DELIMITER ;

/*function to check if pokemon is favorite*/
DELIMITER //
CREATE PROCEDURE is_favorite
(IN id INT, IN str text)
BEGIN
	SELECT nat_ID, name, type1, type2, img_url
	FROM pokemon_form_all
  WHERE favorite = TRUE
  AND nat_ID = id
  AND name = str;
END //
DELIMITER ;

/*function to get all popular pokemons*/
DELIMITER //
CREATE PROCEDURE get_populars()
BEGIN
	SELECT nat_ID, name, link
	FROM poke_pop;
END //
DELIMITER ;

/*count how many favorite pokemons*/
DELIMITER //
CREATE PROCEDURE count_favorite()
BEGIN
	SELECT COUNT(nat_ID)
    FROM pokemon_form_all
    WHERE favorite = TRUE;
END //
DELIMITER ;


/*
INSERT VALUES INTO TABLES
*/

INSERT INTO poke_pop (nat_ID, name, link) VALUES (407,'Roserade', 'https://www.pokemon.com/us/pokedex/roserade');
INSERT INTO poke_pop (nat_ID, name, link) VALUES (428,'Lopunny', 'https://www.pokemon.com/us/pokedex/lopunny');
INSERT INTO poke_pop (nat_ID, name, link) VALUES (429,'Mismagius', 'https://www.pokemon.com/us/pokedex/mismagius');
INSERT INTO poke_pop (nat_ID, name, link) VALUES (461,'Weavile', 'https://www.pokemon.com/us/pokedex/weavile');
INSERT INTO poke_pop (nat_ID, name, link) VALUES (462,'Magnezone', 'https://www.pokemon.com/us/pokedex/magnezone');
INSERT INTO poke_pop (nat_ID, name, link) VALUES (464,'Rhyperior', 'https://www.pokemon.com/us/pokedex/rhyperior');
INSERT INTO poke_pop (nat_ID, name, link) VALUES (465,'Tangrowth', 'https://www.pokemon.com/us/pokedex/tangrowth');
INSERT INTO poke_pop (nat_ID, name, link) VALUES (466,'Electivire', 'https://www.pokemon.com/us/pokedex/electivire');
INSERT INTO poke_pop (nat_ID, name, link) VALUES (467,'Magmortar', 'https://www.pokemon.com/us/pokedex/magmortar');
INSERT INTO poke_pop (nat_ID, name, link) VALUES (470,'Leafeon', 'https://www.pokemon.com/us/pokedex/leafeon');
INSERT INTO poke_pop (nat_ID, name, link) VALUES (471,'Glaceon', 'https://www.pokemon.com/us/pokedex/glaceon');
INSERT INTO poke_pop (nat_ID, name, link) VALUES (472,'Gliscor', 'https://www.pokemon.com/us/pokedex/gliscor');
INSERT INTO poke_pop (nat_ID, name, link) VALUES (392,'Infernape', 'https://www.pokemon.com/us/pokedex/infernape');
INSERT INTO poke_pop (nat_ID, name, link) VALUES (475,'Gallade', 'https://www.pokemon.com/us/pokedex/gallade');
INSERT INTO poke_pop (nat_ID, name, link) VALUES (483,'Dialga', 'https://www.pokemon.com/us/pokedex/dialga');


INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (446,NULL, 'Munchlax', 'NORMAL', 135,85,40,40,85,5,390, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/253/215/199/446.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (438,NULL, 'Bonsly', 'ROCK', 50,80,95,10,45,10,290, 'ROCK', NULL, 6,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/245/215/199/438.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (597,NULL, 'Ferroseed', 'GRASS;STEEL', 44,50,91,24,86,10,305, 'GRASS', 'STEEL', 12,9, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/404/215/199/597.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (524,NULL, 'Roggenrola', 'ROCK', 55,75,85,25,25,15,280, 'ROCK', NULL, 6,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/331/215/199/524.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (590,NULL, 'Foongus', 'GRASS;POISON', 69,55,45,55,55,15,294, 'GRASS', 'POISON', 12,4, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/397/215/199/590.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (525,NULL, 'Boldore', 'ROCK', 70,105,105,50,40,20,390, 'ROCK', NULL, 6,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/332/215/199/525.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (577,NULL, 'Solosis', 'PSYCHIC', 45,30,40,105,50,20,290, 'PSYCHIC', NULL, 14,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/384/215/199/577.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (589,NULL, 'Escavalier', 'BUG;STEEL', 70,135,105,60,105,20,495, 'BUG', 'STEEL', 7,9, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/396/215/199/589.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (598,NULL, 'Ferrothorn', 'GRASS;STEEL', 74,94,131,54,116,20,489, 'GRASS', 'STEEL', 12,9, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/405/215/199/598.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (607,NULL, 'Litwick', 'GHOST;FIRE', 50,30,55,65,55,20,275, 'GHOST', 'FIRE', 8,10, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/414/215/199/607.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (564,NULL, 'Tirtouga', 'WATER;ROCK', 54,78,103,53,45,22,355, 'WATER', 'ROCK', 11,6, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/371/215/199/564.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (436,NULL, 'Bronzor', 'STEEL;PSYCHIC', 57,24,86,24,86,23,300, 'STEEL', 'PSYCHIC', 9,14, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/243/215/199/436.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (517,NULL, 'Munna', 'PSYCHIC', 76,25,45,67,55,24,292, 'PSYCHIC', NULL, 14,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/324/215/199/517.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (401,NULL, 'Kricketot', 'BUG', 37,25,41,25,41,25,194, 'BUG', NULL, 7,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/208/215/199/401.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (526,NULL, 'Gigalith', 'ROCK', 85,135,130,60,80,25,515, 'ROCK', NULL, 6,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/333/215/199/526.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (616,NULL, 'Shelmet', 'BUG', 50,40,85,40,65,25,305, 'BUG', NULL, 7,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/423/215/199/616.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (518,NULL, 'Musharna', 'PSYCHIC', 116,55,85,107,95,29,487, 'PSYCHIC', NULL, 14,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/325/215/199/518.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (410,NULL, 'Shieldon', 'ROCK;STEEL', 30,42,118,42,88,30,350, 'ROCK', 'STEEL', 6,9, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/217/215/199/410.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (411,NULL, 'Bastiodon', 'ROCK;STEEL', 60,52,168,47,138,30,495, 'ROCK', 'STEEL', 6,9, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/218/215/199/411.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (440,NULL, 'Happiny', 'NORMAL', 100,5,5,15,65,30,220, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/247/215/199/440.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (548,NULL, 'Petilil', 'GRASS', 45,35,50,70,50,30,280, 'GRASS', NULL, 12,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/355/215/199/548.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (562,NULL, 'Yamask', 'GHOST', 38,30,85,55,65,30,303, 'GHOST', NULL, 8,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/369/215/199/562.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (563,NULL, 'Cofagrigus', 'GHOST', 58,50,145,95,105,30,483, 'GHOST', NULL, 8,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/370/215/199/563.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (578,NULL, 'Duosion', 'PSYCHIC', 65,40,50,125,60,30,370, 'PSYCHIC', NULL, 14,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/385/215/199/578.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (579,NULL, 'Reuniclus', 'PSYCHIC', 110,65,75,125,85,30,490, 'PSYCHIC', NULL, 14,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/386/215/199/579.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (591,NULL, 'Amoonguss', 'GRASS;POISON', 114,85,70,85,80,30,464, 'GRASS', 'POISON', 12,4, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/398/215/199/591.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (599,NULL, 'Klink', 'STEEL', 40,55,70,45,60,30,300, 'STEEL', NULL, 9,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/406/215/199/599.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (605,NULL, 'Elgyem', 'PSYCHIC', 55,55,55,85,55,30,335, 'PSYCHIC', NULL, 14,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/412/215/199/605.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (387,NULL, 'Turtwig', 'GRASS', 55,68,64,45,55,31,318, 'GRASS', NULL, 12,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/194/215/199/387.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (399,NULL, 'Bidoof', 'NORMAL', 59,45,40,35,40,31,250, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/206/215/199/399.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (449,NULL, 'Hippopotas', 'GROUND', 68,72,78,38,42,32,330, 'GROUND', NULL, 5,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/256/215/199/449.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (565,NULL, 'Carracosta', 'WATER;ROCK', 74,108,133,83,65,32,495, 'WATER', 'ROCK', 11,6, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/372/215/199/565.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (618,NULL, 'Stunfisk', 'GROUND;ELECTRIC', 109,66,84,81,99,32,471, 'GROUND', 'ELECTRIC', 5,13, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/425/215/199/618.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (437,NULL, 'Bronzong', 'STEEL;PSYCHIC', 67,89,116,79,116,33,500, 'STEEL', 'PSYCHIC', 9,14, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/244/215/199/437.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (422,NULL, 'Shellos', 'WATER', 76,48,48,57,62,34,325, 'WATER', NULL, 11,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/229/215/199/422.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (420,NULL, 'Cherubi', 'GRASS', 45,35,45,62,53,35,275, 'GRASS', NULL, 12,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/227/215/199/420.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (442,NULL, 'Spiritomb', 'GHOST;DARK', 50,92,108,92,108,35,485, 'GHOST', 'DARK', 8,18, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/249/215/199/442.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (532,NULL, 'Timburr', 'FIGHT', 75,80,55,25,35,35,305, 'FIGHT', NULL, 2,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/339/215/199/532.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (622,NULL, 'Golett', 'GROUND;GHOST', 59,74,50,35,50,35,303, 'GROUND', 'GHOST', 5,8, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/429/215/199/622.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (388,NULL, 'Grotle', 'GRASS', 75,89,85,55,65,36,405, 'GRASS', NULL, 12,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/195/215/199/388.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (412,NULL, 'Burmy', 'BUG', 40,29,45,29,45,36,224, 'BUG', NULL, 7,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/219/215/199/412.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (413,NULL, 'Wormadam', 'BUG;GRASS', 60,59,85,79,105,36,424, 'BUG', 'GRASS', 7,12, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/220/215/199/413.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (423,NULL, 'Gastrodon', 'WATER;GROUND', 111,83,68,92,82,39,475, 'WATER', 'GROUND', 11,5, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/230/215/199/423.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (393,NULL, 'Piplup', 'WATER', 53,51,53,61,56,40,314, 'WATER', NULL, 11,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/200/215/199/393.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (416,NULL, 'Vespiquen', 'BUG;FLYING', 70,80,102,80,102,40,474, 'BUG', 'FLYING', 7,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/223/215/199/416.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (459,NULL, 'Snover', 'GRASS;ICE', 60,62,50,62,60,40,334, 'GRASS', 'ICE', 12,15, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/266/215/199/459.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (464,178, 'Rhyperior', 'GROUND;ROCK', 115,140,130,55,55,40,535, 'GROUND', 'ROCK', 5,6, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/271/215/199/464.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (476,62, 'Probopass', 'ROCK;STEEL', 60,55,145,75,150,40,525, 'ROCK', 'STEEL', 6,9, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/283/215/199/476.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (533,NULL, 'Gurdurr', 'FIGHT', 85,105,85,40,50,40,405, 'FIGHT', NULL, 2,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/340/215/199/533.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (592,NULL, 'Frillish', 'WATER;GHOST', 55,40,50,65,85,40,335, 'WATER', 'GHOST', 11,8, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/399/215/199/592.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (603,NULL, 'Eelektrik', 'ELECTRIC', 65,85,70,75,70,40,405, 'ELECTRIC', NULL, 13,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/410/215/199/603.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (606,NULL, 'Beheeyem', 'PSYCHIC', 75,75,75,125,95,40,485, 'PSYCHIC', NULL, 14,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/413/215/199/606.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (613,NULL, 'Cubchoo', 'ICE', 55,70,40,60,40,40,305, 'ICE', NULL, 15,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/420/215/199/613.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (443,NULL, 'Gible', 'DRAGON;GROUND', 58,70,45,40,45,42,300, 'DRAGON', 'GROUND', 16,5, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/250/215/199/443.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (504,NULL, 'Patrat', 'NORMAL', 45,55,39,35,39,42,255, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/311/215/199/504.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (540,NULL, 'Sewaddle', 'BUG;GRASS', 45,53,70,40,60,42,310, 'BUG', 'GRASS', 7,12, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/347/215/199/540.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (541,NULL, 'Swadloon', 'BUG;GRASS', 55,63,90,50,80,42,380, 'BUG', 'GRASS', 7,12, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/348/215/199/541.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (519,NULL, 'Pidove', 'NORMAL;FLYING', 50,55,50,36,30,43,264, 'NORMAL', 'FLYING', 1,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/326/215/199/519.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (582,NULL, 'Vanillite', 'ICE', 36,50,50,65,60,44,305, 'ICE', NULL, 15,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/389/215/199/582.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (403,NULL, 'Shinx', 'ELECTRIC', 45,65,34,40,34,45,263, 'ELECTRIC', NULL, 13,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/210/215/199/403.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (433,157, 'Chingling', 'PSYCHIC', 45,30,50,65,50,45,285, 'PSYCHIC', NULL, 14,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/240/215/199/433.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (477,155, 'Dusknoir', 'GHOST', 45,100,135,65,135,45,525, 'GHOST', NULL, 8,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/284/215/199/477.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (498,NULL, 'Tepig', 'FIRE', 65,63,45,45,45,45,308, 'FIRE', NULL, 10,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/305/215/199/498.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (501,NULL, 'Oshawott', 'WATER', 55,55,45,63,45,45,308, 'WATER', NULL, 11,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/308/215/199/501.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (534,NULL, 'Conkeldurr', 'FIGHT', 105,140,95,55,65,45,505, 'FIGHT', NULL, 2,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/341/215/199/534.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (538,NULL, 'Throh', 'FIGHT', 120,100,85,30,85,45,465, 'FIGHT', NULL, 2,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/345/215/199/538.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (558,NULL, 'Crustle', 'BUG;ROCK', 70,95,125,65,75,45,475, 'BUG', 'ROCK', 7,6, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/365/215/199/558.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (574,NULL, 'Gothita', 'PSYCHIC', 45,30,50,55,65,45,290, 'PSYCHIC', NULL, 14,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/381/215/199/574.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (455,NULL, 'Carnivine', 'GRASS', 74,100,72,90,72,46,454, 'GRASS', NULL, 12,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/262/215/199/455.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (450,NULL, 'Hippowdon', 'GROUND', 108,112,118,68,72,47,525, 'GROUND', NULL, 5,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/257/215/199/450.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (544,NULL, 'Whirlipede', 'BUG;POISON', 40,55,99,40,79,47,360, 'BUG', 'POISON', 7,4, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/351/215/199/544.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (559,NULL, 'Scraggy', 'DARK;FIGHT', 50,75,70,35,70,48,348, 'DARK', 'FIGHT', 18,2, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/366/215/199/559.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (621,NULL, 'Druddigon', 'DRAGON', 77,120,90,60,90,48,485, 'DRAGON', NULL, 16,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/428/215/199/621.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (394,NULL, 'Prinplup', 'WATER', 64,66,68,81,76,50,405, 'WATER', NULL, 11,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/201/215/199/394.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (453,NULL, 'Croagunk', 'POISON;FIGHT', 48,61,40,61,40,50,300, 'POISON', 'FIGHT', 4,2, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/260/215/199/453.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (458,NULL, 'Mantyke', 'WATER;FLYING', 45,20,50,60,120,50,345, 'WATER', 'FLYING', 11,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/265/215/199/458.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (463,NULL, 'Lickilicky', 'NORMAL', 110,85,95,80,95,50,515, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/270/215/199/463.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (465,NULL, 'Tangrowth', 'GRASS', 100,100,125,110,50,50,535, 'GRASS', NULL, 12,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/272/215/199/465.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (531,NULL, 'Audino', 'NORMAL', 103,60,86,60,86,50,445, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/338/215/199/531.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (554,NULL, 'Darumaka', 'FIRE', 70,90,45,15,45,50,315, 'FIRE', NULL, 10,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/361/215/199/554.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (600,NULL, 'Klang', 'STEEL', 60,80,95,70,85,50,440, 'STEEL', NULL, 9,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/407/215/199/600.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (604,NULL, 'Eelektross', 'ELECTRIC', 85,115,80,105,80,50,515, 'ELECTRIC', NULL, 13,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/411/215/199/604.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (614,NULL, 'Beartic', 'ICE', 95,110,80,70,80,50,485, 'ICE', NULL, 15,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/421/215/199/614.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (406,97, 'Budew', 'GRASS;POISON', 40,30,35,50,70,55,280, 'GRASS', 'POISON', 12,4, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/213/215/199/406.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (499,NULL, 'Pignite', 'FIRE;FIGHT', 90,93,55,70,55,55,418, 'FIRE', 'FIGHT', 10,2, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/306/215/199/499.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (506,NULL, 'Lillipup', 'NORMAL', 45,60,45,25,45,55,275, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/313/215/199/506.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (557,NULL, 'Dwebble', 'BUG;ROCK', 50,65,85,35,35,55,325, 'BUG', 'ROCK', 7,6, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/364/215/199/557.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (575,NULL, 'Gothorita', 'PSYCHIC', 60,45,70,75,85,55,390, 'PSYCHIC', NULL, 14,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/382/215/199/575.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (580,NULL, 'Ducklett', 'WATER;FLYING', 62,44,50,44,50,55,305, 'WATER', 'FLYING', 11,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/387/215/199/580.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (608,NULL, 'Lampent', 'GHOST;FIRE', 60,40,60,95,60,55,370, 'GHOST', 'FIRE', 8,10, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/415/215/199/608.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (623,NULL, 'Golurk', 'GROUND;GHOST', 89,124,80,55,80,55,483, 'GROUND', 'GHOST', 5,8, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/430/215/199/623.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (626,NULL, 'Bouffalant', 'NORMAL', 95,110,95,40,95,55,490, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/433/215/199/626.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (389,NULL, 'Torterra', 'GRASS;GROUND', 95,109,105,75,85,56,525, 'GRASS', 'GROUND', 12,5, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/196/215/199/389.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (543,NULL, 'Venipede', 'BUG;POISON', 30,45,59,30,39,57,260, 'BUG', 'POISON', 7,4, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/350/215/199/543.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (610,NULL, 'Axew', 'DRAGON', 46,87,60,30,40,57,320, 'DRAGON', NULL, 16,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/417/215/199/610.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (408,NULL, 'Cranidos', 'ROCK', 67,125,40,30,30,58,350, 'ROCK', NULL, 6,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/215/215/199/408.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (409,NULL, 'Rampardos', 'ROCK', 97,165,60,65,50,58,495, 'ROCK', NULL, 6,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/216/215/199/409.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (560,NULL, 'Scrafty', 'DARK;FIGHT', 65,90,115,45,115,58,488, 'DARK', 'FIGHT', 18,2, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/367/215/199/560.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (583,NULL, 'Vanillish', 'ICE', 51,65,65,80,75,59,395, 'ICE', NULL, 15,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/390/215/199/583.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (395,NULL, 'Empoleon', 'WATER;STEEL', 84,86,88,111,101,60,530, 'WATER', 'STEEL', 11,9, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/202/215/199/395.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (396,NULL, 'Starly', 'NORMAL;FLYING', 40,55,30,30,30,60,245, 'NORMAL', 'FLYING', 1,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/203/215/199/396.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (404,NULL, 'Luxio', 'ELECTRIC', 60,85,49,60,49,60,363, 'ELECTRIC', NULL, 13,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/211/215/199/404.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (439,NULL, 'Mime Jr.', 'PSYCHIC;FAIRY', 20,25,45,70,90,60,310, 'PSYCHIC', 'FAIRY', 14,17, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/246/215/199/439.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (447,NULL, 'Riolu', 'FIGHT', 40,70,40,35,40,60,285, 'FIGHT', NULL, 2,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/254/215/199/447.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (460,NULL, 'Abomasnow', 'GRASS;ICE', 90,92,75,92,85,60,494, 'GRASS', 'ICE', 12,15, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/267/215/199/460.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (462,86, 'Magnezone', 'ELECTRIC;STEEL', 70,70,115,130,90,60,535, 'ELECTRIC', 'STEEL', 13,9, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/269/215/199/462.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (502,NULL, 'Dewott', 'WATER', 75,75,60,83,60,60,413, 'WATER', NULL, 11,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/309/215/199/502.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (507,NULL, 'Herdier', 'NORMAL', 65,80,65,35,65,60,370, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/314/215/199/507.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (556,NULL, 'Maractus', 'GRASS', 75,86,67,106,67,60,461, 'GRASS', NULL, 12,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/363/215/199/556.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (588,NULL, 'Karrablast', 'BUG', 50,75,45,40,45,60,315, 'BUG', NULL, 7,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/395/215/199/588.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (593,NULL, 'Jellicent', 'WATER;GHOST', 100,60,70,85,105,60,480, 'WATER', 'GHOST', 11,8, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/400/215/199/593.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (602,NULL, 'Tynamo', 'ELECTRIC', 35,55,40,45,40,60,275, 'ELECTRIC', NULL, 13,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/409/215/199/602.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (624,NULL, 'Pawniard', 'DARK;STEEL', 45,85,70,40,40,60,340, 'DARK', 'STEEL', 18,9, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/431/215/199/624.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (627,NULL, 'Rufflet', 'NORMAL;FLYING', 70,83,50,37,50,60,350, 'NORMAL', 'FLYING', 1,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/434/215/199/627.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (390,NULL, 'Chimchar', 'FIRE', 44,58,44,58,44,61,309, 'FIRE', NULL, 10,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/197/215/199/390.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (495,NULL, 'Snivy', 'GRASS', 45,45,55,45,55,63,308, 'GRASS', NULL, 12,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/302/215/199/495.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (511,NULL, 'Pansage', 'GRASS', 50,53,48,53,48,64,316, 'GRASS', NULL, 12,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/318/215/199/511.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (513,NULL, 'Pansear', 'FIRE', 50,53,48,53,48,64,316, 'FIRE', NULL, 10,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/320/215/199/513.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (515,NULL, 'Panpour', 'WATER', 50,53,48,53,48,64,316, 'WATER', NULL, 11,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/322/215/199/515.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (535,NULL, 'Tympole', 'WATER', 50,50,40,50,40,64,294, 'WATER', NULL, 11,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/342/215/199/535.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (402,NULL, 'Kricketune', 'BUG', 77,85,51,55,51,65,384, 'BUG', NULL, 7,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/209/215/199/402.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (451,NULL, 'Skorupi', 'POISON;BUG', 40,50,90,30,55,65,330, 'POISON', 'BUG', 4,7, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/258/215/199/451.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (471,NULL, 'Glaceon', 'ICE', 65,60,110,130,95,65,525, 'ICE', NULL, 15,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/278/215/199/471.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (500,NULL, 'Emboar', 'FIRE;FIGHT', 110,123,65,100,65,65,528, 'FIRE', 'FIGHT', 10,2, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/307/215/199/500.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (520,NULL, 'Tranquill', 'NORMAL;FLYING', 62,77,62,50,42,65,358, 'NORMAL', 'FLYING', 1,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/327/215/199/520.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (551,NULL, 'Sandile', 'GROUND;DARK', 50,72,35,35,35,65,292, 'GROUND', 'DARK', 5,18, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/358/215/199/551.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (568,NULL, 'Trubbish', 'POISON', 50,50,62,40,62,65,329, 'POISON', NULL, 4,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/375/215/199/568.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (570,NULL, 'Zorua', 'DARK', 40,65,40,80,40,65,330, 'DARK', NULL, 18,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/377/215/199/570.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (576,NULL, 'Gothitelle', 'PSYCHIC', 70,55,95,95,110,65,490, 'PSYCHIC', NULL, 14,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/383/215/199/576.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (594,NULL, 'Alomomola', 'WATER', 165,75,80,40,45,65,470, 'WATER', NULL, 11,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/401/215/199/594.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (595,NULL, 'Joltik', 'BUG;ELECTRIC', 50,47,50,57,50,65,319, 'BUG', 'ELECTRIC', 7,13, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/402/215/199/595.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (619,NULL, 'Mienfoo', 'FIGHT', 45,85,50,55,50,65,350, 'FIGHT', NULL, 2,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/426/215/199/619.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (414,NULL, 'Mothim', 'BUG;FLYING', 70,94,50,94,50,66,424, 'BUG', 'FLYING', 7,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/221/215/199/414.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (456,NULL, 'Finneon', 'WATER', 49,49,56,49,61,66,330, 'WATER', NULL, 11,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/263/215/199/456.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (509,NULL, 'Purrloin', 'DARK', 41,50,37,50,37,66,281, 'DARK', NULL, 18,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/316/215/199/509.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (546,NULL, 'Cottonee', 'GRASS;FAIRY', 40,27,60,37,50,66,280, 'GRASS', 'FAIRY', 12,17, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/353/215/199/546.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (611,NULL, 'Fraxure', 'DRAGON', 66,117,70,40,50,67,410, 'DRAGON', NULL, 16,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/418/215/199/611.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (529,NULL, 'Drilbur', 'GROUND', 60,85,40,30,45,68,328, 'GROUND', NULL, 5,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/336/215/199/529.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (536,NULL, 'Palpitoad', 'WATER;GROUND', 75,65,55,65,55,69,384, 'WATER', 'GROUND', 11,5, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/343/215/199/536.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (405,NULL, 'Luxray', 'ELECTRIC', 80,120,79,95,79,70,523, 'ELECTRIC', NULL, 13,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/212/215/199/405.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (415,NULL, 'Combee', 'BUG;FLYING', 30,30,42,30,42,70,244, 'BUG', 'FLYING', 7,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/222/215/199/415.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (425,NULL, 'Drifloon', 'GHOST;FLYING', 90,50,34,60,44,70,348, 'GHOST', 'FLYING', 8,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/232/215/199/425.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (503,NULL, 'Samurott', 'WATER', 95,100,85,108,70,70,528, 'WATER', NULL, 11,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/310/215/199/503.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (566,NULL, 'Archen', 'ROCK;FLYING', 55,112,45,74,45,70,401, 'ROCK', 'FLYING', 6,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/373/215/199/566.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (625,NULL, 'Bisharp', 'DARK;STEEL', 65,125,100,60,70,70,490, 'DARK', 'STEEL', 18,9, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/432/215/199/625.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (400,NULL, 'Bibarel', 'NORMAL;WATER', 79,85,60,55,60,71,410, 'NORMAL', 'WATER', 1,11, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/207/215/199/400.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (430,NULL, 'Honchkrow', 'DARK;FLYING', 100,125,52,105,52,71,505, 'DARK', 'FLYING', 18,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/237/215/199/430.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (527,NULL, 'Woobat', 'PSYCHIC;FLYING', 55,45,43,55,43,72,313, 'PSYCHIC', 'FLYING', 14,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/334/215/199/527.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (434,NULL, 'Stunky', 'POISON;DARK', 63,63,47,41,41,74,329, 'POISON', 'DARK', 4,18, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/241/215/199/434.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (537,NULL, 'Seismitoad', 'WATER;GROUND', 105,95,75,85,75,74,509, 'WATER', 'GROUND', 11,5, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/344/215/199/537.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (552,NULL, 'Krokorok', 'GROUND;DARK', 60,82,45,45,45,74,351, 'GROUND', 'DARK', 5,18, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/359/215/199/552.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (569,NULL, 'Garbodor', 'POISON', 80,95,82,60,82,75,474, 'POISON', NULL, 4,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/376/215/199/569.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (572,NULL, 'Minccino', 'NORMAL', 55,50,40,40,40,75,300, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/379/215/199/572.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (585,NULL, 'Deerling', 'NORMAL;GRASS', 60,60,50,40,50,75,335, 'NORMAL', 'GRASS', 1,12, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/392/215/199/585.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (522,NULL, 'Blitzle', 'ELECTRIC', 45,60,32,50,32,76,295, 'ELECTRIC', NULL, 13,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/329/215/199/522.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (485,NULL, 'Heatran', 'FIRE;STEEL', 91,90,106,130,106,77,600, 'FIRE', 'STEEL', 10,9, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/292/215/199/485.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (505,NULL, 'Watchog', 'NORMAL', 60,85,69,60,69,77,420, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/312/215/199/505.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (584,NULL, 'Vanilluxe', 'ICE', 71,95,85,110,95,79,535, 'ICE', NULL, 15,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/391/215/199/584.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (397,NULL, 'Staravia', 'NORMAL;FLYING', 55,75,50,40,40,80,340, 'NORMAL', 'FLYING', 1,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/204/215/199/397.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (426,NULL, 'Drifblim', 'GHOST;FLYING', 150,80,44,90,54,80,498, 'GHOST', 'FLYING', 8,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/233/215/199/426.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (468,NULL, 'Togekiss', 'FAIRY;FLYING', 85,50,95,120,115,80,545, 'FAIRY', 'FLYING', 17,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/275/215/199/468.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (473,NULL, 'Mamoswine', 'ICE;GROUND', 110,130,80,70,60,80,530, 'ICE', 'GROUND', 15,5, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/280/215/199/473.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (475,32, 'Gallade', 'PSYCHIC;FIGHT', 68,125,65,65,115,80,518, 'PSYCHIC', 'FIGHT', 14,2, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/282/215/199/475.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (481,NULL, 'Mesprit', 'PSYCHIC', 80,105,105,105,105,80,580, 'PSYCHIC', NULL, 14,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/288/215/199/481.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (489,NULL, 'Phione', 'WATER', 80,80,80,80,80,80,480, 'WATER', NULL, 11,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/296/215/199/489.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (508,NULL, 'Stoutland', 'NORMAL', 85,110,90,45,90,80,500, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/315/215/199/508.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (609,NULL, 'Chandelure', 'GHOST;FIRE', 60,55,90,145,90,80,520, 'GHOST', 'FIRE', 8,10, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/416/215/199/609.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (391,NULL, 'Monferno', 'FIRE;FIGHT', 64,78,52,78,52,81,405, 'FIRE', 'FIGHT', 10,2, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/198/215/199/391.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (444,NULL, 'Gabite', 'DRAGON;GROUND', 68,90,65,50,55,82,410, 'DRAGON', 'GROUND', 16,5, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/251/215/199/444.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (467,NULL, 'Magmortar', 'FIRE', 75,95,67,125,95,83,540, 'FIRE', NULL, 10,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/274/215/199/467.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (496,NULL, 'Servine', 'GRASS', 60,60,75,60,75,83,413, 'GRASS', NULL, 12,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/303/215/199/496.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (435,NULL, 'Skuntank', 'POISON;DARK', 103,93,67,71,61,84,479, 'POISON', 'DARK', 4,18, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/242/215/199/435.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (418,NULL, 'Buizel', 'WATER', 55,65,35,60,30,85,330, 'WATER', NULL, 11,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/225/215/199/418.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (421,NULL, 'Cherrim', 'GRASS', 70,60,70,87,78,85,450, 'GRASS', NULL, 12,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/228/215/199/421.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (427,NULL, 'Buneary', 'NORMAL', 55,66,44,44,56,85,350, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/234/215/199/427.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (431,NULL, 'Glameow', 'NORMAL', 49,55,42,42,37,85,310, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/238/215/199/431.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (454,NULL, 'Toxicroak', 'POISON;FIGHT', 83,106,65,86,65,85,490, 'POISON', 'FIGHT', 4,2, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/261/215/199/454.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (488,NULL, 'Cresselia', 'PSYCHIC', 120,70,120,75,130,85,600, 'PSYCHIC', NULL, 14,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/295/215/199/488.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (539,NULL, 'Sawk', 'FIGHT', 75,125,75,30,75,85,465, 'FIGHT', NULL, 2,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/346/215/199/539.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (530,NULL, 'Excadrill', 'GROUND;STEEL', 110,135,60,50,65,88,508, 'GROUND', 'STEEL', 5,9, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/337/215/199/530.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (407,99, 'Roserade', 'GRASS;POISON', 60,70,65,125,105,90,515, 'GRASS', 'POISON', 12,4, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/214/215/199/407.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (448,NULL, 'Lucario', 'FIGHT;STEEL', 70,110,70,115,70,90,525, 'FIGHT', 'STEEL', 2,9, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/255/215/199/448.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (474,NULL, 'Porygon-Z', 'NORMAL', 85,80,70,135,75,90,535, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/281/215/199/474.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (483,NULL, 'Dialga', 'STEEL;DRAGON', 100,120,120,150,100,90,680, 'STEEL', 'DRAGON', 9,16, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/290/215/199/483.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (487,NULL, 'Giratina', 'GHOST;DRAGON', 150,100,120,100,120,90,680, 'GHOST', 'DRAGON', 8,16, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/294/215/199/487.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (549,NULL, 'Lilligant', 'GRASS', 70,60,75,110,75,90,480, 'GRASS', NULL, 12,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/356/215/199/549.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (601,NULL, 'Klinklang', 'STEEL', 60,100,115,70,85,90,520, 'STEEL', NULL, 9,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/408/215/199/601.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (441,NULL, 'Chatot', 'NORMAL;FLYING', 76,65,45,92,42,91,411, 'NORMAL', 'FLYING', 1,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/248/215/199/441.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (457,NULL, 'Lumineon', 'WATER', 69,69,76,69,86,91,460, 'WATER', NULL, 11,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/264/215/199/457.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (479,NULL, 'Rotom', 'ELECTRIC;GHOST', 50,50,77,95,77,91,440, 'ELECTRIC', 'GHOST', 13,8, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/286/215/199/479.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (542,NULL, 'Leavanny', 'BUG;GRASS', 75,103,80,70,80,92,500, 'BUG', 'GRASS', 7,12, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/349/215/199/542.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (553,NULL, 'Krookodile', 'GROUND;DARK', 95,117,80,65,70,92,519, 'GROUND', 'DARK', 5,18, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/360/215/199/553.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (521,NULL, 'Unfezant', 'NORMAL;FLYING', 80,115,80,65,55,93,488, 'NORMAL', 'FLYING', 1,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/328/215/199/521.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (417,NULL, 'Pachirisu', 'ELECTRIC', 60,45,70,45,90,95,405, 'ELECTRIC', NULL, 13,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/224/215/199/417.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (452,NULL, 'Drapion', 'POISON;DARK', 70,90,110,60,75,95,500, 'POISON', 'DARK', 4,18, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/259/215/199/452.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (466,NULL, 'Electivire', 'ELECTRIC', 75,123,67,95,85,95,540, 'ELECTRIC', NULL, 13,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/273/215/199/466.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (469,NULL, 'Yanmega', 'BUG;FLYING', 86,76,86,116,56,95,515, 'BUG', 'FLYING', 7,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/276/215/199/469.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (470,NULL, 'Leafeon', 'GRASS', 65,110,130,60,65,95,525, 'GRASS', NULL, 12,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/277/215/199/470.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (472,NULL, 'Gliscor', 'GROUND;FLYING', 75,95,125,45,75,95,510, 'GROUND', 'FLYING', 5,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/279/215/199/472.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (480,NULL, 'Uxie', 'PSYCHIC', 75,75,130,75,130,95,580, 'PSYCHIC', NULL, 14,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/287/215/199/480.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (555,NULL, 'Darmanitan', 'FIRE', 105,140,55,30,55,95,480, 'FIRE', NULL, 10,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/362/215/199/555.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (586,NULL, 'Sawsbuck', 'NORMAL;GRASS', 80,100,70,60,70,95,475, 'NORMAL', 'GRASS', 1,12, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/393/215/199/586.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (561,NULL, 'Sigilyph', 'PSYCHIC;FLYING', 72,58,80,103,80,97,490, 'PSYCHIC', 'FLYING', 14,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/368/215/199/561.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (612,NULL, 'Haxorus', 'DRAGON', 76,147,90,60,70,97,540, 'DRAGON', NULL, 16,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/419/215/199/612.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (550,NULL, 'Basculin', 'WATER', 70,92,65,80,55,98,460, 'WATER', NULL, 11,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/357/215/199/550.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (581,NULL, 'Swanna', 'WATER;FLYING', 75,87,63,87,63,98,473, 'WATER', 'FLYING', 11,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/388/215/199/581.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (398,NULL, 'Staraptor', 'NORMAL;FLYING', 85,120,70,50,60,100,485, 'NORMAL', 'FLYING', 1,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/205/215/199/398.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (484,NULL, 'Palkia', 'WATER;DRAGON', 90,120,100,150,120,100,680, 'WATER', 'DRAGON', 11,16, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/291/215/199/484.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (486,NULL, 'Regigigas', 'NORMAL', 110,160,110,80,110,100,670, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/293/215/199/486.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (490,NULL, 'Manaphy', 'WATER', 100,100,100,100,100,100,600, 'WATER', NULL, 11,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/297/215/199/490.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (492,NULL, 'Shaymin', 'GRASS', 100,100,100,100,100,100,600, 'GRASS', NULL, 12,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/299/215/199/492.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (494,NULL, 'Victini', 'PSYCHIC;FIRE', 100,100,100,100,100,100,600, 'PSYCHIC', 'FIRE', 14,10, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/301/215/199/494.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (512,NULL, 'Simisage', 'GRASS', 75,98,63,98,63,101,498, 'GRASS', NULL, 12,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/319/215/199/512.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (514,NULL, 'Simisear', 'FIRE', 75,98,63,98,63,101,498, 'FIRE', NULL, 10,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/321/215/199/514.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (516,NULL, 'Simipour', 'WATER', 75,98,63,98,63,101,498, 'WATER', NULL, 11,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/323/215/199/516.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (445,NULL, 'Garchomp', 'DRAGON;GROUND', 108,130,95,80,85,102,600, 'DRAGON', 'GROUND', 16,5, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/252/215/199/445.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (587,NULL, 'Emolga', 'ELECTRIC;FLYING', 55,75,60,75,60,103,428, 'ELECTRIC', 'FLYING', 13,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/394/215/199/587.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (428,NULL, 'Lopunny', 'NORMAL', 65,76,84,54,96,105,480, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/235/215/199/428.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (429,NULL, 'Mismagius', 'GHOST', 60,60,60,105,105,105,495, 'GHOST', NULL, 8,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/236/215/199/429.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (571,NULL, 'Zoroark', 'DARK', 60,105,60,120,60,105,510, 'DARK', NULL, 18,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/378/215/199/571.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (615,NULL, 'Cryogonal', 'ICE', 70,50,30,95,135,105,485, 'ICE', NULL, 15,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/422/215/199/615.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (620,NULL, 'Mienshao', 'FIGHT', 65,125,60,95,60,105,510, 'FIGHT', NULL, 2,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/427/215/199/620.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (510,NULL, 'Liepard', 'DARK', 64,88,50,88,50,106,446, 'DARK', NULL, 18,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/317/215/199/510.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (392,NULL, 'Infernape', 'FIRE;FIGHT', 76,104,71,104,71,108,534, 'FIRE', 'FIGHT', 10,2, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/199/215/199/392.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (596,NULL, 'Galvantula', 'BUG;ELECTRIC', 70,77,60,97,60,108,472, 'BUG', 'ELECTRIC', 7,13, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/403/215/199/596.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (478,181, 'Froslass', 'ICE;GHOST', 70,80,70,80,70,110,480, 'ICE', 'GHOST', 15,8, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/285/215/199/478.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (567,NULL, 'Archeops', 'ROCK;FLYING', 75,140,65,112,65,110,567, 'ROCK', 'FLYING', 6,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/374/215/199/567.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (432,NULL, 'Purugly', 'NORMAL', 71,82,64,64,59,112,452, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/239/215/199/432.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (545,NULL, 'Scolipede', 'BUG;POISON', 60,100,89,55,69,112,485, 'BUG', 'POISON', 7,4, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/352/215/199/545.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (497,NULL, 'Serperior', 'GRASS', 75,75,95,75,95,113,528, 'GRASS', NULL, 12,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/304/215/199/497.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (528,NULL, 'Swoobat', 'PSYCHIC;FLYING', 67,57,55,77,55,114,425, 'PSYCHIC', 'FLYING', 14,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/335/215/199/528.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (419,NULL, 'Floatzel', 'WATER', 85,105,55,85,50,115,495, 'WATER', NULL, 11,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/226/215/199/419.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (424,NULL, 'Ambipom', 'NORMAL', 75,100,66,60,66,115,482, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/231/215/199/424.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (482,NULL, 'Azelf', 'PSYCHIC', 75,125,70,125,70,115,580, 'PSYCHIC', NULL, 14,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/289/215/199/482.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (573,NULL, 'Cinccino', 'NORMAL', 75,95,60,65,60,115,470, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/380/215/199/573.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (523,NULL, 'Zebstrika', 'ELECTRIC', 75,100,63,80,63,116,497, 'ELECTRIC', NULL, 13,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/330/215/199/523.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (547,NULL, 'Whimsicott', 'GRASS;FAIRY', 60,67,85,77,75,116,480, 'GRASS', 'FAIRY', 12,17, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/354/215/199/547.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (493,NULL, 'Arceus', 'NORMAL', 120,120,120,120,120,120,720, 'NORMAL', NULL, 1,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/300/215/199/493.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (461,NULL, 'Weavile', 'DARK;ICE', 70,120,65,45,85,125,510, 'DARK', 'ICE', 18,15, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/268/215/199/461.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (491,NULL, 'Darkrai', 'DARK', 70,90,90,135,90,125,600, 'DARK', NULL, 18,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/298/215/199/491.png', FALSE);
INSERT INTO pokemon (nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (617,NULL, 'Accelgor', 'BUG', 80,70,40,100,60,145,495, 'BUG', NULL, 7,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/424/215/199/617.png', FALSE);


INSERT INTO poke_form (form_ID, nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (1, 413,NULL, 'Wormadam (Sandy Cloak)', 'BUG;GROUND', 60,79,105,59,85,36,424, 'BUG', 'GROUND', 7,5, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/323/443/215/199/41401.png', FALSE);
INSERT INTO poke_form (form_ID, nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (2, 413,NULL, 'Wormadam (Trash Cloak)', 'BUG;STEEL', 60,69,95,69,95,36,424, 'BUG', 'STEEL', 7,9, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/323/444/215/199/41402.png', FALSE);
INSERT INTO poke_form (form_ID, nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (3, 421,NULL, 'Cherrim (Sunshine Form)', 'GRASS', 70,60,70,87,78,85,450, 'GRASS', NULL, 12,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/323/446/215/199/42201.png', FALSE);
INSERT INTO poke_form (form_ID, nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (4, 479,NULL, 'Rotom (Heat Rotom)', 'ELECTRIC;FIRE', 50,65,107,105,107,86,520, 'ELECTRIC', 'FIRE', 13,10, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/323/448/215/199/48001.png', FALSE);
INSERT INTO poke_form (form_ID, nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (5, 479,NULL, 'Rotom (Fan Rotom)', 'ELECTRIC;FLYING', 50,65,107,105,107,86,520, 'ELECTRIC', 'FLYING', 13,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/323/451/215/199/48004.png', FALSE);
INSERT INTO poke_form (form_ID, nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (6, 479,NULL, 'Rotom (Mow Rotom)', 'ELECTRIC;GRASS', 50,65,107,105,107,86,520, 'ELECTRIC', 'GRASS', 13,12, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/323/452/215/199/48005.png', FALSE);
INSERT INTO poke_form (form_ID, nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (7, 479,NULL, 'Rotom (Frost Rotom)', 'ELECTRIC;ICE', 50,65,107,105,107,86,520, 'ELECTRIC', 'ICE', 13,15, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/323/450/215/199/48003.png', FALSE);
INSERT INTO poke_form (form_ID, nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (8, 479,NULL, 'Rotom (Wash Rotom)', 'ELECTRIC;WATER', 50,65,107,105,107,86,520, 'ELECTRIC', 'WATER', 13,11, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/323/449/215/199/48002.png', FALSE);
INSERT INTO poke_form (form_ID, nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (9, 487,NULL, 'Giratina (Origin Forme)', 'GHOST;DRAGON', 150,120,100,120,100,90,680, 'GHOST', 'DRAGON', 8,16, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/323/453/215/199/48801.png', FALSE);
INSERT INTO poke_form (form_ID, nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (10, 492,NULL, 'Shaymin (Sky Forme)', 'GRASS;FLYING', 100,103,75,120,75,127,600, 'GRASS', 'FLYING', 12,3, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/323/454/215/199/49301.png', FALSE);
INSERT INTO poke_form (form_ID, nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (11, 550,NULL, 'Basculin (Blue-Striped Form)', 'WATER', 70,92,65,80,55,98,460, 'WATER', NULL, 11,NULL, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/278/897/215/199/550.png', FALSE);
INSERT INTO poke_form (form_ID, nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (12, 555,NULL, 'Darmanitan (Zen Mode)', 'FIRE;PSYCHIC', 105,30,105,140,105,55,540, 'FIRE', 'PSYCHIC', 10,14, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/323/456/215/199/55601.png', FALSE);
INSERT INTO poke_form (form_ID, nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (13, 428,NULL, 'Mega Lopunny', 'NORMAL;FIGHT', 65,136,94,54,96,135,580, 'NORMAL', 'FIGHT', 1,2, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/323/447/215/199/42901.png', FALSE);
INSERT INTO poke_form (form_ID, nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (14, 445,NULL, 'Mega Garchomp', 'DRAGON;GROUND', 108,170,115,120,95,92,700, 'DRAGON', 'GROUND', 16,5, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/323/434/215/199/10058.png', FALSE);
INSERT INTO poke_form (form_ID, nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (15, 448,NULL, 'Mega Lucario', 'FIGHT;STEEL', 70,145,88,140,70,112,625, 'FIGHT', 'STEEL', 2,9, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/323/424/215/199/100076.png', FALSE);
INSERT INTO poke_form (form_ID, nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (16, 460,NULL, 'Mega Abomasnow', 'GRASS;ICE', 90,132,105,132,105,30,594, 'GRASS', 'ICE', 12,15, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/323/435/215/199/10060.png', FALSE);
INSERT INTO poke_form (form_ID, nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (17, 475,32, 'Mega Gallade', 'PSYCHIC;FIGHT', 68,165,95,65,115,110,618, 'PSYCHIC', 'FIGHT', 14,2, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/320/503/215/199/635532020063317332.png', FALSE);
INSERT INTO poke_form (form_ID, nat_ID, hoenn_ID, name, type, HP, ATK, DEF, SAT, SDF, SPD, BST, type1, type2, type1_ID, type2_ID, img_url, favorite) VALUES (18, 531,NULL, 'Mega Audino', 'NORMAL;FAIRY', 103,60,126,80,126,50,545, 'NORMAL', 'FAIRY', 1,17, 'https://media-cerulean.cursecdn.com/avatars/thumbnails/323/455/215/199/53201.png', FALSE);


INSERT INTO type (type_ID, type, strong, weak, resistant, vulnerable) VALUES (1, 'NORMAL', NULL, 'Rock, Ghost, Steel', 'Ghost', 'Fighting');
INSERT INTO type (type_ID, type, strong, weak, resistant, vulnerable) VALUES (2, 'FIGHT', 'Normal, Rock, Steel, Ice, Dark', 'Flying, Poison, Psychic, Bug, Ghost, Fairy', 'Rock, Bug, Dark', 'Flying, Psychic, Fairy');
INSERT INTO type (type_ID, type, strong, weak, resistant, vulnerable) VALUES (3, 'FLYING', 'Fighting, Bug, Grass', 'Rock, Steel, Electric', 'Fighting, Ground, Bug, Grass', 'Rock, Electric, Ice');
INSERT INTO type (type_ID, type, strong, weak, resistant, vulnerable) VALUES (4, 'POISON', 'Grass, Fairy', 'Poison, Ground, Rock, Ghost, Steel', 'Fighting, Poison, Grass, Fairy', 'Ground, Psychic');
INSERT INTO type (type_ID, type, strong, weak, resistant, vulnerable) VALUES (5, 'GROUND', 'Poison, Rock, Steel, Fire, Electric', 'Flying, Bug, Grass', 'Poison, Rock, Electric', 'Water, Grass, Ice');
INSERT INTO type (type_ID, type, strong, weak, resistant, vulnerable) VALUES (6, 'ROCK', 'Flying, Bug, Fire, Ice', 'Fighting, Ground, Steel', 'Normal, Flying, Poison, Fire', 'Fighting, Ground, Steel, Water, Grass');
INSERT INTO type (type_ID, type, strong, weak, resistant, vulnerable) VALUES (7, 'BUG', 'Grass, Psychic, Dark', 'Fighting, Flying, Poison, Ghost, Steel, Fire, Fairy', 'Fighting, Ground, Grass', 'Flying, Rock, Fire');
INSERT INTO type (type_ID, type, strong, weak, resistant, vulnerable) VALUES (8, 'GHOST', 'Ghost, Psychic', 'Normal, Dark', 'Normal, Fighting, Poison, Bug', 'Ghost, Dark');
INSERT INTO type (type_ID, type, strong, weak, resistant, vulnerable) VALUES (9, 'STEEL', 'Rock, Ice, Fairy', 'Steel, Fire, Water, Electric', 'Normal, Flying, Poison, Rock, Bug, Steel, Grass, Psychic, Ice, Dragon, Fairy', 'Fighting, Ground, Fire');
INSERT INTO type (type_ID, type, strong, weak, resistant, vulnerable) VALUES (10, 'FIRE', 'Bug, Steel, Grass, Ice', 'Rock, Fire, Water, Dragon', 'Bug, Steel, Fire, Grass, Ice', 'Ground, Rock, Water');
INSERT INTO type (type_ID, type, strong, weak, resistant, vulnerable) VALUES (11, 'WATER', 'Ground, Rock, Fire', 'Water, Grass, Dragon', 'Steel, Fire, Water, Ice', 'Grass, Electric');
INSERT INTO type (type_ID, type, strong, weak, resistant, vulnerable) VALUES (12, 'GRASS', 'Ground, Rock, Water', 'Flying, Poison, Bug, Steel, Fire, Grass, Dragon', 'Ground, Water, Grass, Electric', 'Flying, Poison, Bug, Fire, Ice');
INSERT INTO type (type_ID, type, strong, weak, resistant, vulnerable) VALUES (13, 'ELECTRIC', 'Flying, Water', 'Ground, Grass, Electric, Dragon', 'Flying, Steel, Electric', 'Ground');
INSERT INTO type (type_ID, type, strong, weak, resistant, vulnerable) VALUES (14, 'PSYCHIC', 'Fighting, Poison', 'Steel, Psychic, Dark', 'Fighting, Psychic', 'Bug, Ghost, Dark');
INSERT INTO type (type_ID, type, strong, weak, resistant, vulnerable) VALUES (15, 'ICE', 'Flying, Ground, Grass, Dragon', 'Steel, Fire, Water, Ice', 'Ice', 'Fighting, Rock, Steel, Fire');
INSERT INTO type (type_ID, type, strong, weak, resistant, vulnerable) VALUES (16, 'DRAGON', 'Dragon', 'Steel, Fairy', 'Fire, Water, Grass, Electric', 'Ice, Dragon, Fairy');
INSERT INTO type (type_ID, type, strong, weak, resistant, vulnerable) VALUES (17, 'FAIRY', 'Fighting, Dragon, Dark', 'Poison, Steel, Fire', 'Fighting, Bug, Dragon, Dark', 'Poison, Steel');
INSERT INTO type (type_ID, type, strong, weak, resistant, vulnerable) VALUES (18, 'DARK', 'Ghost, Psychic', 'Fighting, Dark, Fairy', 'Ghost, Psychic, Dark', 'Fighting, Bug, Fairy');


INSERT INTO poke_type (poke_ID, type_ID) VALUES (387,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (388,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (389,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (389,5);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (390,10);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (391,10);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (391,2);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (392,10);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (392,2);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (393,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (394,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (395,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (395,9);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (396,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (396,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (397,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (397,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (398,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (398,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (399,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (400,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (400,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (401,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (402,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (403,13);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (404,13);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (405,13);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (406,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (406,4);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (407,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (407,4);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (408,6);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (409,6);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (410,6);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (410,9);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (411,6);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (411,9);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (412,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (413,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (413,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (414,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (414,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (415,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (415,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (416,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (416,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (417,13);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (418,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (419,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (420,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (421,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (422,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (423,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (423,5);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (424,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (425,8);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (425,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (426,8);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (426,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (427,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (428,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (429,8);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (430,18);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (430,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (431,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (432,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (433,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (434,4);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (434,18);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (435,4);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (435,18);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (436,9);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (436,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (437,9);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (437,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (438,6);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (439,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (439,17);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (440,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (441,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (441,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (442,8);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (442,18);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (443,16);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (443,5);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (444,16);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (444,5);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (445,16);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (445,5);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (446,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (447,2);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (448,2);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (448,9);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (449,5);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (450,5);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (451,4);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (451,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (452,4);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (452,18);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (453,4);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (453,2);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (454,4);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (454,2);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (455,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (456,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (457,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (458,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (458,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (459,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (459,15);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (460,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (460,15);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (461,18);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (461,15);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (462,13);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (462,9);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (463,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (464,5);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (464,6);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (465,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (466,13);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (467,10);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (468,17);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (468,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (469,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (469,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (470,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (471,15);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (472,5);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (472,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (473,15);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (473,5);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (474,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (475,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (475,2);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (476,6);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (476,9);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (477,8);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (478,15);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (478,8);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (479,13);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (479,10);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (480,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (481,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (482,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (483,9);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (483,16);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (484,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (484,16);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (485,10);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (485,9);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (486,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (487,8);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (487,16);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (488,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (489,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (490,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (491,18);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (492,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (492,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (493,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (494,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (494,10);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (495,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (496,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (497,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (498,10);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (499,10);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (499,2);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (500,10);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (500,2);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (501,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (502,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (503,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (504,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (505,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (506,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (507,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (508,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (509,18);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (510,18);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (511,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (512,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (513,10);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (514,10);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (515,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (516,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (517,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (518,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (519,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (519,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (520,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (520,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (521,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (521,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (522,13);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (523,13);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (524,6);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (525,6);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (526,6);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (527,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (527,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (528,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (528,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (529,5);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (530,5);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (530,9);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (531,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (532,2);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (533,2);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (534,2);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (535,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (536,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (536,5);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (537,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (537,5);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (538,2);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (539,2);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (540,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (540,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (541,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (541,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (542,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (542,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (543,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (543,4);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (544,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (544,4);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (545,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (545,4);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (546,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (546,17);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (547,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (547,17);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (548,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (549,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (550,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (551,5);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (551,18);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (552,5);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (552,18);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (553,5);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (553,18);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (554,10);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (555,10);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (555,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (556,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (557,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (557,6);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (558,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (558,6);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (559,18);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (559,2);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (560,18);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (560,2);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (561,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (561,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (562,8);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (563,8);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (564,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (564,6);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (565,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (565,6);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (566,6);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (566,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (567,6);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (567,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (568,4);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (569,4);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (570,18);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (571,18);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (572,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (573,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (574,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (575,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (576,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (577,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (578,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (579,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (580,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (580,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (581,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (581,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (582,15);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (583,15);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (584,15);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (585,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (585,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (586,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (586,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (587,13);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (587,3);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (588,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (589,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (589,9);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (590,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (590,4);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (591,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (591,4);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (592,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (592,8);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (593,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (593,8);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (594,11);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (595,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (595,13);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (596,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (596,13);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (597,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (597,9);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (598,12);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (598,9);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (599,9);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (600,9);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (601,9);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (602,13);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (603,13);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (604,13);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (605,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (606,14);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (607,8);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (607,10);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (608,8);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (608,10);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (609,8);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (609,10);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (610,16);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (611,16);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (612,16);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (613,15);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (614,15);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (615,15);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (616,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (617,7);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (618,5);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (618,13);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (619,2);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (620,2);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (621,16);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (622,5);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (622,8);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (623,5);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (623,8);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (624,18);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (624,9);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (625,18);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (625,9);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (626,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (627,1);
INSERT INTO poke_type (poke_ID, type_ID) VALUES (627,3);

INSERT INTO strong (type_ID, strong_ID) VALUES (2,1);
INSERT INTO strong (type_ID, strong_ID) VALUES (2,6);
INSERT INTO strong (type_ID, strong_ID) VALUES (2,9);
INSERT INTO strong (type_ID, strong_ID) VALUES (2,15);
INSERT INTO strong (type_ID, strong_ID) VALUES (2,18);
INSERT INTO strong (type_ID, strong_ID) VALUES (3,2);
INSERT INTO strong (type_ID, strong_ID) VALUES (3,7);
INSERT INTO strong (type_ID, strong_ID) VALUES (3,12);
INSERT INTO strong (type_ID, strong_ID) VALUES (4,12);
INSERT INTO strong (type_ID, strong_ID) VALUES (4,17);
INSERT INTO strong (type_ID, strong_ID) VALUES (5,4);
INSERT INTO strong (type_ID, strong_ID) VALUES (5,6);
INSERT INTO strong (type_ID, strong_ID) VALUES (5,9);
INSERT INTO strong (type_ID, strong_ID) VALUES (5,10);
INSERT INTO strong (type_ID, strong_ID) VALUES (5,13);
INSERT INTO strong (type_ID, strong_ID) VALUES (6,3);
INSERT INTO strong (type_ID, strong_ID) VALUES (6,7);
INSERT INTO strong (type_ID, strong_ID) VALUES (6,10);
INSERT INTO strong (type_ID, strong_ID) VALUES (6,15);
INSERT INTO strong (type_ID, strong_ID) VALUES (7,12);
INSERT INTO strong (type_ID, strong_ID) VALUES (7,14);
INSERT INTO strong (type_ID, strong_ID) VALUES (7,18);
INSERT INTO strong (type_ID, strong_ID) VALUES (8,8);
INSERT INTO strong (type_ID, strong_ID) VALUES (8,14);
INSERT INTO strong (type_ID, strong_ID) VALUES (9,6);
INSERT INTO strong (type_ID, strong_ID) VALUES (9,15);
INSERT INTO strong (type_ID, strong_ID) VALUES (9,17);
INSERT INTO strong (type_ID, strong_ID) VALUES (10,7);
INSERT INTO strong (type_ID, strong_ID) VALUES (10,9);
INSERT INTO strong (type_ID, strong_ID) VALUES (10,12);
INSERT INTO strong (type_ID, strong_ID) VALUES (10,15);
INSERT INTO strong (type_ID, strong_ID) VALUES (11,5);
INSERT INTO strong (type_ID, strong_ID) VALUES (11,6);
INSERT INTO strong (type_ID, strong_ID) VALUES (11,10);
INSERT INTO strong (type_ID, strong_ID) VALUES (12,5);
INSERT INTO strong (type_ID, strong_ID) VALUES (12,6);
INSERT INTO strong (type_ID, strong_ID) VALUES (12,11);
INSERT INTO strong (type_ID, strong_ID) VALUES (13,3);
INSERT INTO strong (type_ID, strong_ID) VALUES (13,11);
INSERT INTO strong (type_ID, strong_ID) VALUES (14,2);
INSERT INTO strong (type_ID, strong_ID) VALUES (14,4);
INSERT INTO strong (type_ID, strong_ID) VALUES (15,3);
INSERT INTO strong (type_ID, strong_ID) VALUES (15,5);
INSERT INTO strong (type_ID, strong_ID) VALUES (15,12);
INSERT INTO strong (type_ID, strong_ID) VALUES (15,16);
INSERT INTO strong (type_ID, strong_ID) VALUES (16,16);
INSERT INTO strong (type_ID, strong_ID) VALUES (17,2);
INSERT INTO strong (type_ID, strong_ID) VALUES (17,16);
INSERT INTO strong (type_ID, strong_ID) VALUES (17,18);
INSERT INTO strong (type_ID, strong_ID) VALUES (18,8);
INSERT INTO strong (type_ID, strong_ID) VALUES (18,14);

INSERT INTO weak (type_ID, weak_ID) VALUES (1,6);
INSERT INTO weak (type_ID, weak_ID) VALUES (1,8);
INSERT INTO weak (type_ID, weak_ID) VALUES (1,9);
INSERT INTO weak (type_ID, weak_ID) VALUES (2,3);
INSERT INTO weak (type_ID, weak_ID) VALUES (2,4);
INSERT INTO weak (type_ID, weak_ID) VALUES (2,14);
INSERT INTO weak (type_ID, weak_ID) VALUES (2,7);
INSERT INTO weak (type_ID, weak_ID) VALUES (2,8);
INSERT INTO weak (type_ID, weak_ID) VALUES (2,17);
INSERT INTO weak (type_ID, weak_ID) VALUES (3,6);
INSERT INTO weak (type_ID, weak_ID) VALUES (3,9);
INSERT INTO weak (type_ID, weak_ID) VALUES (3,13);
INSERT INTO weak (type_ID, weak_ID) VALUES (4,4);
INSERT INTO weak (type_ID, weak_ID) VALUES (4,5);
INSERT INTO weak (type_ID, weak_ID) VALUES (4,6);
INSERT INTO weak (type_ID, weak_ID) VALUES (4,8);
INSERT INTO weak (type_ID, weak_ID) VALUES (4,9);
INSERT INTO weak (type_ID, weak_ID) VALUES (5,3);
INSERT INTO weak (type_ID, weak_ID) VALUES (5,7);
INSERT INTO weak (type_ID, weak_ID) VALUES (5,12);
INSERT INTO weak (type_ID, weak_ID) VALUES (6,2);
INSERT INTO weak (type_ID, weak_ID) VALUES (6,5);
INSERT INTO weak (type_ID, weak_ID) VALUES (6,9);
INSERT INTO weak (type_ID, weak_ID) VALUES (7,2);
INSERT INTO weak (type_ID, weak_ID) VALUES (7,3);
INSERT INTO weak (type_ID, weak_ID) VALUES (7,4);
INSERT INTO weak (type_ID, weak_ID) VALUES (7,8);
INSERT INTO weak (type_ID, weak_ID) VALUES (7,9);
INSERT INTO weak (type_ID, weak_ID) VALUES (7,10);
INSERT INTO weak (type_ID, weak_ID) VALUES (7,17);
INSERT INTO weak (type_ID, weak_ID) VALUES (8,1);
INSERT INTO weak (type_ID, weak_ID) VALUES (8,18);
INSERT INTO weak (type_ID, weak_ID) VALUES (9,9);
INSERT INTO weak (type_ID, weak_ID) VALUES (9,10);
INSERT INTO weak (type_ID, weak_ID) VALUES (9,11);
INSERT INTO weak (type_ID, weak_ID) VALUES (9,13);
INSERT INTO weak (type_ID, weak_ID) VALUES (10,6);
INSERT INTO weak (type_ID, weak_ID) VALUES (10,10);
INSERT INTO weak (type_ID, weak_ID) VALUES (10,11);
INSERT INTO weak (type_ID, weak_ID) VALUES (10,16);
INSERT INTO weak (type_ID, weak_ID) VALUES (11,11);
INSERT INTO weak (type_ID, weak_ID) VALUES (11,12);
INSERT INTO weak (type_ID, weak_ID) VALUES (11,16);
INSERT INTO weak (type_ID, weak_ID) VALUES (12,3);
INSERT INTO weak (type_ID, weak_ID) VALUES (12,4);
INSERT INTO weak (type_ID, weak_ID) VALUES (12,7);
INSERT INTO weak (type_ID, weak_ID) VALUES (12,9);
INSERT INTO weak (type_ID, weak_ID) VALUES (12,10);
INSERT INTO weak (type_ID, weak_ID) VALUES (12,12);
INSERT INTO weak (type_ID, weak_ID) VALUES (12,16);
INSERT INTO weak (type_ID, weak_ID) VALUES (13,5);
INSERT INTO weak (type_ID, weak_ID) VALUES (13,12);
INSERT INTO weak (type_ID, weak_ID) VALUES (13,13);
INSERT INTO weak (type_ID, weak_ID) VALUES (13,16);
INSERT INTO weak (type_ID, weak_ID) VALUES (14,9);
INSERT INTO weak (type_ID, weak_ID) VALUES (14,14);
INSERT INTO weak (type_ID, weak_ID) VALUES (14,18);
INSERT INTO weak (type_ID, weak_ID) VALUES (15,9);
INSERT INTO weak (type_ID, weak_ID) VALUES (15,10);
INSERT INTO weak (type_ID, weak_ID) VALUES (15,11);
INSERT INTO weak (type_ID, weak_ID) VALUES (15,15);
INSERT INTO weak (type_ID, weak_ID) VALUES (16,9);
INSERT INTO weak (type_ID, weak_ID) VALUES (16,17);
INSERT INTO weak (type_ID, weak_ID) VALUES (17,4);
INSERT INTO weak (type_ID, weak_ID) VALUES (17,9);
INSERT INTO weak (type_ID, weak_ID) VALUES (17,10);
INSERT INTO weak (type_ID, weak_ID) VALUES (18,2);
INSERT INTO weak (type_ID, weak_ID) VALUES (18,18);
INSERT INTO weak (type_ID, weak_ID) VALUES (18,17);

INSERT INTO resistant (type_ID, resistant_ID) VALUES (1,8);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (2,6);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (2,7);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (2,18);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (3,2);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (3,5);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (3,7);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (3,12);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (4,2);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (4,4);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (4,12);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (4,17);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (5,4);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (5,6);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (5,13);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (6,1);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (6,3);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (6,4);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (6,10);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (7,2);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (7,5);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (7,12);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (8,1);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (8,2);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (8,4);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (8,7);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (9,1);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (9,3);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (9,4);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (9,6);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (9,7);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (9,9);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (9,12);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (9,14);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (9,15);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (9,16);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (9,17);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (10,7);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (10,9);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (10,10);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (10,12);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (10,15);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (11,9);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (11,10);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (11,11);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (11,15);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (12,5);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (12,11);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (12,12);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (12,13);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (13,3);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (13,9);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (13,13);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (14,2);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (14,14);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (15,15);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (16,10);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (16,11);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (16,12);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (16,13);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (17,2);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (17,7);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (17,16);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (17,18);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (18,8);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (18,14);
INSERT INTO resistant (type_ID, resistant_ID) VALUES (18,18);

INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (1,2);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (2,3);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (2,14);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (2,17);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (3,6);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (3,13);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (3,15);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (4,5);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (4,14);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (5,11);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (5,12);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (5,15);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (6,2);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (6,5);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (6,9);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (6,11);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (6,12);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (7,3);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (7,6);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (7,10);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (8,8);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (8,18);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (9,2);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (9,5);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (9,10);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (10,5);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (10,6);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (10,11);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (11,12);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (11,13);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (12,3);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (12,4);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (12,7);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (12,10);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (12,15);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (13,5);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (14,7);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (14,8);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (14,18);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (15,2);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (15,6);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (15,9);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (15,10);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (16,15);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (16,16);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (16,17);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (17,4);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (17,9);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (18,2);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (18,7);
INSERT INTO vulnerable (type_ID, vulnerable_ID) VALUES (18,17);
