/*
SET DATABASE AS DEFAULT
-- For some reason, when using the "Import" function in phpMyAdmin, the command below, which should set the database 'pokedex' as default doesn't work.
As a work around, before importing the Queries.sql file, please select the 'pokedex' database in phpMyAdmin.
*/
USE pokedex;

/*
1.Find the Bottom 10 Pokémon ranked by speed |
"ORDER BY" will bring records sorted by smallest to gratest
*/
SELECT * FROM pokemon_form_all
ORDER BY SPD
LIMIT 30;

/*
2.Find All Pokémon who is vulnerable to Ground and Resistant to Steel |
"WHERE" and "AND" clauses use "IN" function in order to achieve precise filtering on the self referenced tables
*/
SELECT DISTINCT p.nat_ID, p.name, p.type
FROM ((pokemon_form_all p
       INNER JOIN poke_type pt ON p.nat_ID = pt.poke_ID)
				  INNER JOIN type t ON pt.type_ID = t.type_ID)
WHERE t.type_ID IN (SELECT v.type_ID FROM vulnerable v INNER JOIN type t ON v.type_ID = t.type_ID WHERE v.vulnerable_ID = (SELECT t.type_ID FROM type t WHERE t.type = "GROUND"))
AND t.type_ID IN (SELECT r.type_ID FROM resistant r INNER JOIN type t ON r.type_ID = t.type_ID WHERE r.resistant_ID = (SELECT t.type_ID FROM type t WHERE t.type = "STEEL"))
ORDER BY p.nat_ID;

/*
3.Find All Pokémon who has a BST between 200 to 500 who is weak against water types |
"AND" clause uses "IN" function in order to achieve precise filtering on the self referenced tables
*/
SELECT DISTINCT p.nat_ID, p.name, p.type, p.BST
FROM ((pokemon_form_all p
       INNER JOIN poke_type pt ON p.nat_ID = pt.poke_ID)
				  INNER JOIN type t ON pt.type_ID = t.type_ID)
WHERE (p.BST BETWEEN 200 AND 500)
AND t.type_ID IN (SELECT w.type_ID FROM weak w INNER JOIN type t ON w.type_ID = t.type_ID WHERE w.weak_ID = (SELECT t.type_ID FROM type t WHERE t.type = "WATER"))
ORDER BY p.nat_ID;

/*
4.Find the Pokémon with the highest Atk, has a Mega evolution form and vulnerable to fire |
"WHERE" clause use "IN" function to identify the pokemon who has a "Mega" evolution form
"AND" clause uses "IN" function in order to achieve precise filtering on the self referenced tables
*/
SELECT p.nat_ID, p.name, p.type, p.ATK, t.vulnerable
FROM ((pokemon_form_all p
       INNER JOIN poke_type pt ON p.nat_ID = pt.poke_ID)
				  INNER JOIN type t ON pt.type_ID = t.type_ID)
WHERE p.nat_ID IN (SELECT p.nat_ID
					FROM pokemon p INNER JOIN poke_form pf ON p.nat_ID = pf.nat_ID WHERE pf.name LIKE '%Mega%')
AND t.type_ID IN (SELECT v.type_ID FROM vulnerable v INNER JOIN type t ON v.type_ID = t.type_ID WHERE v.vulnerable_ID = (SELECT t.type_ID FROM type t WHERE t.type = "FIRE"))
ORDER BY p.ATK DESC
LIMIT 1;
