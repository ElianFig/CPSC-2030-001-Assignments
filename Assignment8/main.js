$(document).ready(function(){
    let item = $(".items");
    let bullet = $(".bullet");
    function menuClick(event) {
        let menu = $(".menuBar");
        let items = $(".items");
        let mP = $(".mainP");
        menu.toggleClass("rotate");
        items.toggleClass("show");
        mP.toggleClass("noMargin");
        bullet.removeClass();
        bullet.toggleClass("bullet menu");
     }
     function item1Click(event) {
         item.removeClass();
         item.toggleClass("items show item1");
         bullet.removeClass();
         bullet.toggleClass("bullet it1");
     }
     function item2Click(event) {
         item.removeClass();
         item.toggleClass("items show item2");
         bullet.removeClass();
         bullet.toggleClass("bullet it2");
     }
     function item3Click(event) {
         item.removeClass();
         item.toggleClass("items show item3");
         bullet.removeClass();
         bullet.toggleClass("bullet it3");
     }
     function item4Click(event) {
         item.removeClass();
         item.toggleClass("items show item4");
         bullet.removeClass();
         bullet.toggleClass("bullet it4");
     }
     function item5Click(event) {
         item.removeClass();
         item.toggleClass("items show item5");
         bullet.removeClass();
         bullet.toggleClass("bullet it5");
     }

     $(".menuBar").click(menuClick);
     $(".item1").click(item1Click);
     $(".item2").click(item2Click);
     $(".item3").click(item3Click);
     $(".item4").click(item4Click);
     $(".item5").click(item5Click);
});
