<?php

//Functions to help with server-client requests
//require_once './vendor/autoload.php';  //include the twig library.
function clearConnection($mysql){
    while($mysql->more_results()){
       $mysql->next_result();
       $mysql->use_result();
    }
}

//open and close tags
function wrap($tag,$value) { //expect strings for both parameters
    return "<$tag>$value</$tag>";
}

//DB setup for Assignment5
function DBConnect(){
    $user = 'CPSC2030';
    $pwd = 'CPSC2030';
    $server = 'localhost:4001';
    $dbname = 'pokedex';

    $conn = new mysqli($server, $user, $pwd, $dbname);
    // Check connection
    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }
    return $conn;
}
?>
