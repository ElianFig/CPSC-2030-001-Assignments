<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pokedex</title>
    <link rel="stylesheet/less" type="text/css" media="screen" href="style.less" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  </head>
  <body>
    <div class="bg">
      <div class="grid-container">
        <div id="menu" class="menu">
            <a href="index.php"><div>Home</div></a>
        </div>
        <div class="main">
            <h1>POKEDEX</h1>
            <?php
            include 'sqlUseful.php';
            $conn = DBConnect();
            if (isset($_GET["type"])) {
                $type = mysqli_real_escape_string($conn, $_GET["type"]); //prevent SQL injection
                $result = $conn->query("call pokemon_by_type(\"$type\")");
            }
            else {
                $result = $conn->query("CALL get_pokemon()");
            }
            ?>
            <div class="flex-container">
                <?php
                if($result){
                $table = $result->fetch_all(MYSQLI_ASSOC);
                foreach( $table as $row){
                    echo "<div class='pokemon'>";
                    $link = "details.php?poke=".urlencode($row["nat_ID"]);
                    $url = $row["img_url"];
                    $name = $row["name"];
                    echo "<a href='$link'>";
                    echo "<img src='$url' alt='$name'>";
                    echo "</a>";
                    echo wrap("h2",$row["nat_ID"]);
                    echo wrap("h3",$row["name"]);
                    echo "<div class='types'>";
                    $link = "index.php?type=".urlencode($row["type1"]);
                    echo "<a href= '$link'>";
                    echo wrap("p","<i class='fas fa-search'></i>".$row["type1"]);
                    echo "</a>";
                    if ($row["type2"]) {
                        $link = "index.php?type=".urlencode($row["type2"]);
                        echo "<a href='$link'>";
                        echo wrap("p","<i class='fas fa-search'></i>".$row["type2"]);
                        echo "</a>";
                    }
                    echo "</div>";
                    $link = "details.php?poke=".urlencode($row["nat_ID"]);
                    echo "<a class='editButton' href='$link'>Details</a>";
                    echo "</div>";
                }
                $conn->close(); //clean up connection

                }else {
                echo "SQL Error";
                }
                ?>
            </div>
        </div>
      </div>
    </div>
  </body>
</html>
