<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pokemon Details</title>
    <link rel="stylesheet/less" type="text/css" media="screen" href="style.less" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  </head>
  <body>
    <div class="bg">
      <div class="grid-container">
        <div id="menu" class="menu">
            <a href="index.php"><div>Home</div></a>
        </div>
        <div class="main">
            <h1>POKEMON INFO</h1>
            <?php
            include 'sqlUseful.php';
            $conn = DBConnect();
            if (isset($_GET["poke"])) {
                $poke = mysqli_real_escape_string($conn, $_GET["poke"]); //prevent SQL injection
                $result = $conn->query("call pokemon_info(\"$poke\")");
            }
            else {
                $result = "";
            }
            ?>
            <div class="details-container">
                <?php
                //Pokemon details section
                if($result){
                $table = $result->fetch_all(MYSQLI_ASSOC);
                foreach( $table as $row){
                    echo "<div class='poke-detail'>";
                    echo wrap("h1","Pokemon Details");
                    $url = $row["img_url"];
                    $name = $row["name"];
                    echo "<img src='$url' alt='$name'>";
                    echo wrap("h2","ID: ".$row["nat_ID"]);
                    echo wrap("h3 class='name'",$row["name"]);
                    echo wrap("h3","Type: ".$row["type"]);
                    echo "</div>";
                    echo "<div class='poke-stat'>";
                    echo wrap("h1","Pokemon Stats");
                    echo wrap("h3","HP: ".$row["HP"]);
                    echo wrap("h3","ATK: ".$row["ATK"]);
                    echo wrap("h3","DEF: ".$row["DEF"]);
                    echo wrap("h3","SAT: ".$row["SAT"]);
                    echo wrap("h3","SDF: ".$row["SDF"]);
                    echo wrap("h3","SPD: ".$row["SPD"]);
                    echo wrap("h3","BST: ".$row["BST"]);
                    echo "</div>";
                }
                echo "</div>";
                $conn = DBConnect();
                $res_type = $conn->query("call pokemon_type_stat(\"$poke\")");
                //Type details section
                if($res_type){
                    $table = $res_type->fetch_all(MYSQLI_ASSOC);
                    echo wrap("h1","Weaknesses and Resistances");
                    echo "<div class='type-detail'>";
                    foreach( $table as $row){
                        echo wrap("h2",$row["type"]);
                        echo wrap("h3","Strong Against: ".$row["strong"]);
                        echo wrap("h3","Weak Against: ".$row["weak"]);
                        echo wrap("h3","Resistant To: ".$row["resistant"]);
                        echo wrap("h3","Vulnerable To: ".$row["vulnerable"]);
                    }
                    echo "</div>";
                }else {
                    echo wrap("h2", "No results found.");
                }
                $conn = DBConnect();
                $res_form = $conn->query("call pokemon_form(\"$poke\")");
                //Form details section
                if($res_form){
                    $table = $res_form->fetch_all(MYSQLI_ASSOC);
                    echo wrap("h1","Forms and Evolutions");
                    echo "<div class='form-detail'>";
                    foreach( $table as $row){
                        echo "<div class='poke-form'>";
                        $url = $row["img_url"];
                        $name = $row["name"];
                        echo "<img src='$url' alt='$name'>";
                        echo wrap("h2","ID: ".$row["nat_ID"]);
                        echo wrap("h3 class='name'",$row["name"]);
                        echo wrap("h3","Type: ".$row["type"]);
                        echo "</div>";
                    }
                    echo "</div>";
                }
                $conn->close(); //clean up connection

                }else {
                echo wrap("h2", "Please select a pokemon from <a href='index.php'>Home Page</a>");
                }
                ?>
        </div>
      </div>
    </div>
  </body>
</html>
