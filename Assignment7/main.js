//Global variables to be used throught the code
var characters = [];
var squadArr = [];
var battalion;
var squad;
var profile;
var profilePic;

//On load event function
window.onload = function()
{
    //get HTML elements to be use later
    battalion = document.getElementById("battalion");
    squad = document.getElementById("squad");
    profile = document.getElementById("profile");
    profilePic = document.getElementById("charPic");
    //Create characters' array of objects
    buildCharacter();
    //initial call function to write warriors names under Battalion area
    fillBattalion();
    //initial call function to write squad names (empty when page loads)
    fillSquad();
}

//go through each character and display name on page
function fillBattalion(){
    for (var i = 0; i < characters.length; i++) {
        battalion.innerHTML += "<div id='"+ i +"' onmouseover='display("+ i +");' onclick='select("+ i +");'>" + characters[i].name + "</div>";
    }
}

//go through each squad member and display name on page
function fillSquad(){
    var strSquad = "squad";
    squad.innerHTML = "";
    //maximum number os members in the squad is 5. Do not accept duplicate
    for (var s = 0; s < squadArr.length; s++) {
        squad.innerHTML += "<div id='"+ s +"' onmouseover='displayS("+ s +");' onclick='delSquad("+ s +");'>" + squadArr[s].name + "</div>";
    }
}

//displays profile information on page when user hovers over a character
function fillProfile(charID, origin){
    profile.innerHTML = "";
    var auxChar = [];
    //identifying if source is from Squad or Battalion
    if (origin == "squad") {
        auxChar = squadArr;
    } else {
        auxChar = characters;
    }
    //Loop to go through each of the character's properties
    Object.keys(auxChar[charID]).forEach(function(key,index) {
        // key: the name of the object key
        // index: the ordinal position of the key within the object
        //Will display only first 6 properties
        if (index <= 5) {
            profile.innerHTML += "<p><span>"+ key +": </span>"+ auxChar[charID].key(index) +"</p>";
        }
    });
}

//Object Character
function Character(name, side, unit, rank, role, description, image_Url) {
    this.name = name;
    this.side = side;
    this.unit = unit;
    this.rank = rank;
    this.role = role;
    this.description = description;
    this.image_Url = image_Url;
    //Function to return value of each property
    this.key = function(n) {
        return this[Object.keys(this)[n]];
    }
}

//manually creating each character and its properties
function buildCharacter() {
    characters.push(new Character("Claude Wallace", "Edinburgh Army", "Ranger Corps, Squad E", "First Lieutenant", "Tank Commander", "Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.", "http://valkyria.sega.com/img/character/chara-ss01.jpg"));
    characters.push(new Character("Riley Miller", "Edinburgh Army", "Federate Joint Ops", "Second Lieutenant", "Artillery Advisor", "Born in the Gallian city of Hafen, this brilliant inventor was assigned to Squad E after researching ragnite technology in the United States of Vinland. She appears to share some history with Claude, although the memories seem to be traumatic ones.", "http://valkyria.sega.com/img/character/chara-ss02.jpg"));
    characters.push(new Character("Raz", "Edinburgh Army", "Ranger Corps, Squad E", "Sergeant", "Fireteam Leader", "Born in the Gallian city of Hafen, this foul-mouthed Darcsen worked his way up from the slums to become a capable soldier. Though foul-mouthed and reckless, his athleticism and combat prowess is top-notch... And according to him, he's invincible.", "http://valkyria.sega.com/img/character/chara-ss03.jpg"));
    characters.push(new Character("Kai Schulen", "Edinburgh Army", "Ranger Corps, Squad E", "Sergeant Major", "Fireteam Leader", "Born in the Gallian city of Hafen, this cool and collected sharpshooter has earned the codename 'Deadeye Kai'. Along with her childhood friends, she joined a foreign military to take the fight to the Empire. She loves fresh-baked bread, almost to a fault.", "http://valkyria.sega.com/img/character/chara-ss04.jpg"));
    characters.push(new Character("Minerva Victor", "Edinburgh Army", "Ranger Corps, Squad F", "First Lieutenant", "Senior Commander", "Born in the United Kingdom of Edinburgh to a noble family, this competitive perfectionist has authority over the 101st Division's squad leaders. She values honor and chivalry, though a bitter rivalry with Lt. Wallace sometimes compromises her lofty ideals.", "http://valkyria.sega.com/img/character/chara-ss11.jpg"));
    characters.push(new Character("Karen Stuart", "Edinburgh Army", "Squad E", "Corporal", "Combat EMT", "Born as the eldest daughter of a large family, this unflappable field medic is an expert at administering first aid in the heat of battle. Although she had plans to attend medical school, she instead enlisted in her nation's military to support her growing household.", "http://valkyria.sega.com/img/character/chara-ss12.jpg"));
    characters.push(new Character("Ragnarok", "Edinburgh Army", "Squad E", "K-9 Unit", "Mascot", "Once a stray, this good good boy is lovingly referred to as 'Rags'. As a K-9 unit, he's a brave and intelligent rescue dog who's always willing to lend a helping paw. When the going gets tough, the tough get ruff.", "http://valkyria.sega.com/img/character/chara-ss13.jpg"));
    characters.push(new Character("Miles Arbeck", "Edinburgh Army", "Ranger Corps, Squad E", "Sergeant", "Tank Operator", "Born in the United Kingdom of Edinburgh, this excitable driver was Claude Wallace's partner in tank training, and was delighted to be assigned to Squad E. He's taken up photography as a hobby, and is constantly taking snapshots whenever on standby.", "http://valkyria.sega.com/img/character/chara-ss15.jpg"));
    characters.push(new Character("Marie Bennett", "Edinburgh Navy", "Centurion, Cygnus Fleet", "Petty Officer", "Chief of Operations", "As the Centurion's crewmember responsible for overseeing daily operations, this gentle and supportive EWI veteran even takes daily tasks like cooking and cleaning upon herself. She never forgets to wear a smile. Her age is undisclosed, even in her personnel files.", "http://valkyria.sega.com/img/character/chara-ss20.jpg"));
    characters.push(new Character("Louffe", "Edinburgh Navy", "Centurion, Cygnus Fleet", "Sergeant", "Radar Operator", "As the Centurion's crewmember responsible for route reconnaissance, this young Darcsen has a keen intellect and acerbic wit beyond her age. Although her harsh tongue keeps most people at arm's length, she's developed a strong companionship with Marie Bennet.", "http://valkyria.sega.com/img/character/chara-ss21.jpg"));
}

//display profile when source is Battalion
function display(charID) {
    fillProfile(charID, "bat");
    //set picture
    profilePic.src = characters[charID].image_Url;
    profilePic.alt = characters[charID].name;
}

//display profile when source is Squad
function displayS(charID) {
    fillProfile(charID, "squad");
    profilePic.src = squadArr[charID].image_Url;
    profilePic.alt = squadArr[charID].name;
}

//function to select character to be part of the Squad
function select(charID) {
    var div = document.getElementById(charID);
    //Maximum of 5 members allowed in the Squad
    if (squadArr.length < 5 && !(isInSquad(characters[charID]))) {
        //set different class for selected characters
        div.classList.add("selected");
        squadArr.push(characters[charID]);
    }
    //Update Squad display names on page
    fillSquad();
}

//function to bring the ID number from Battalion list
function getBatID(char) {
    return characters.indexOf(char);
}

//function to remove character from Squad
function delSquad(charID) {
    var index = getBatID(squadArr[charID]);
    var div = document.getElementById(index);
    //remove "special" class
    div.classList.remove("selected");
    //remove from Squad array
    squadArr.splice(charID,1);
    //Update Squad display names on page
    fillSquad();
}

//function to verify if a character is already in the Squad
function isInSquad(char) {
    if (squadArr.indexOf(char) >= 0) {
        return true;
    } else {
        return false;
    }
}
