<?php
include "sqlUseful.php";

if(array_key_exists("user", $_POST)){
    $output = array();
    $conn = DBConnect();
    $resRecentMsgs = $conn->query("CALL recent_messages()");
    if (mysqli_num_rows($resRecentMsgs)>0) {
        $output = $resRecentMsgs->fetch_all(MYSQLI_ASSOC);
        echo json_encode($output);
    } else {
        echo json_encode(array());
    }
}

if(array_key_exists("message", $_POST)){
    $conn = DBConnect();
    $userName = mysqli_real_escape_string($conn, $_POST["user"]); //prevent SQL injection
    $conn = DBConnect();
    $msg = mysqli_real_escape_string($conn, $_POST["message"]); //prevent SQL injection
    $conn = DBConnect();
    $resAddMsgs = $conn->query("CALL insert_message(\"$userName\",\"$msg\")");
    if (mysqli_affected_rows($conn) > 0) {
        echo json_encode(array());
    }
}

if(array_key_exists("time", $_POST)){
    $time = $_POST["time"];
    $output = array();
    $conn = DBConnect();
    $resUpdateMsgs = $conn->query("CALL get_messages(\"$time\")");
    if (mysqli_num_rows($resUpdateMsgs)>0) {
        $output = $resUpdateMsgs->fetch_all(MYSQLI_ASSOC);
        echo json_encode($output);
    } else {
        echo json_encode(array());
    }
}


?>
