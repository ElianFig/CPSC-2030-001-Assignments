let currentUser;

/*
Business rules functions -- Start
*/
//Function to have user add username then show chat window
$("#btnChat").click(function(){
    if (($("#username").val() == "") || ($("#username").val().length > 15)) {
        if ($("#username").val() == "") {
            alert("Please insert username!");
        } else {
            alert("Maximum 15 characters allowed.");
        }
        $("#username").focus();
    } else {
        /*let container = document.getElementById("container");
        container.style.gridTemplateAreas = '"main"';
        container.style.gridTemplateRows = "100%";*/
        $("#container").addClass("mainOnly");
        $(".header").css("display", "none");
        $(".main").css("display", "grid");
        //generate ajax request
        let user = $("#username").val();
        currentUser = user;
        getRecentMessages();
        console.log(currentUser);
    }
});

//Function to retrieve last 10 messages on server
function getRecentMessages() {
    $.ajax({
        url: "main.php",
        method: "POST",
        data: {
            user: currentUser
        },
        success: function(msg){
           messageIndex = JSON.parse(msg);
           let output = "";
           let messages = $(".messages");
           messages.empty();
           for(let i = 0; i < messageIndex.length; i++){
              output += createMsgItem(messageIndex[i]);
           }
          messages.html(output);
        }
    })
}

//function to send message to server
function addNewMessage(strMsg) {
    $.ajax({
        url: "main.php",
        method: "POST",
        data: {
            user: currentUser,
            message: strMsg
        },
        success: function(msg){
           //call update messages
           updateMessages();
        }
    })
}

//Function to update messages
function updateMessages() {
    //Check if last message exists
    let lastMsg = Date.parse($(".messages div:last-child > #time").html());
    if (isNaN(lastMsg) == false) {
        lastMsg = $(".messages div:last-child > #time").html();

        $.ajax({
            url: "main.php",
            method: "POST",
            data: {
                time: lastMsg
            },
            success: function(msg){
                messageIndex = JSON.parse(msg);
                let output = "";
                let messages = $(".messages");
                for(let i = 0; i < messageIndex.length; i++){
                   output += createMsgItem(messageIndex[i]);
                }
               messages.append(output);
            }
        })
    }
}

//Send message function (Enter key hit)
$('#inputMessage').keypress(function (e) {
  if (e.which == 13) {
    addNewMessage($('#inputMessage').val());
    $('#inputMessage').val("");
    return false;    //<---- Add this line
  }
});

//Interval function, which will execute every 5 seconds to retrieve new messages from server
window.setInterval(function(){
       //function to update messages
      updateMessages();
}, 5000);  //how long until the function is executed again (5000 = 5 seconds)

/*
Business rules functions -- End
*/


/*
Useful functions -- Start
*/
//create html code reffering to each message
function createMsgItem(row) {
    let msg = "";
    msg = "<div id=\"user\">"+row.username + "</div>" +
          "<div id=\"message\">"+row.message + "</div>" +
          "<div id=\"time\">"+row.sysTime + "</div>";

    if (row.username == currentUser) {
        msg = "<div class=\"item current\">"+msg+"</div>";
    } else {
        msg = "<div class=\"item\">"+msg+"</div>";
    }
    return msg;
}

/*
Useful functions -- End
*/
