/*
CREATE DATABASE
*/
CREATE DATABASE chat;

/*
SET DATABASE AS DEFAULT
*/
USE chat;

/*
CREATE TABLES STATEMENTS
*/
CREATE TABLE chat (
    chat_ID INTEGER NOT NULL AUTO_INCREMENT,
    username CHAR(100) NOT NULL,
    sysTime DATETIME NOT NULL,
    message TEXT NOT NULL,
    PRIMARY KEY (chat_ID)
);

/*
CREATE DB USER
*/
CREATE USER IF NOT EXISTS 'CPSC2030'@'localhost' IDENTIFIED VIA mysql_native_password USING '*F29CF4A1DE8DE0164B8FC2F1BE128965912C97EF';
GRANT ALL PRIVILEGES ON *.* TO 'CPSC2030'@'localhost' REQUIRE NONE WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;

/*
CREATE PROCEDURES
*/
/*function to get messages */
DELIMITER //
CREATE PROCEDURE get_messages
(IN varTime DATETIME)
BEGIN
	SELECT *
	FROM chat
    WHERE sysTime > varTime
    ORDER BY sysTime;
END //
DELIMITER ;

/*function to get top 10 recent messages in the last hour */
DELIMITER //
CREATE PROCEDURE recent_messages()
BEGIN
	SELECT *
	FROM chat
    WHERE sysTime > (NOW() - INTERVAL 1 HOUR)
    ORDER BY sysTime
    LIMIT 10;
END //
DELIMITER ;

/*function to insert a chat message*/
DELIMITER //
CREATE PROCEDURE insert_message
(IN strUser text, IN txtMessage text)
BEGIN
INSERT INTO chat (username, sysTime, message) VALUES (
strUser,
NOW(),
txtMessage
);
END //
DELIMITER ;

/*
INSERT MESSAGE FROM ADMINISTRATOR
*/

INSERT INTO chat (username, sysTime, message) VALUES ("ADMINISTRATOR",NOW(),"Welcome to CharSup! your message will be visible for anyone online. Simply type your message and hit Enter to send. Have fun!")
