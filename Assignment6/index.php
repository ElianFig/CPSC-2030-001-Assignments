<?php
    require_once 'sqlUseful.php';
    require_once './vendor/autoload.php';  //include the twig library.
    $loader = new Twig_Loader_Filesystem('./templates'); //set to load from the ./templates directory

    //Sometimes you have to manually delete the cache
    $twig = new Twig_Environment($loader);

    //If php receives parameters, then it means we need to update the favorite pokemons
    if (isset($_GET["ID"]) and isset($_GET["name"])) {
        $conn = DBConnect();
        $id = mysqli_real_escape_string($conn, $_GET["ID"]); //prevent SQL injection
        $name = mysqli_real_escape_string($conn, $_GET["name"]);

        //query to check if pokemon is favorite
        $conn = DBConnect();
        $resfav = $conn->query("call is_favorite(\"$id\",\"$name\")");

        //if there's records on query above, it means the pokemon is already a favorite one, so we need to delete it
        //otherwise we add it to favorite, if the number of favorite pokemon is less than 7
        if(mysqli_num_rows($resfav)==0){
            //query to check the number of favorite pokemons
            $conn = DBConnect();
            $resfavnum = $conn->query("CALL count_favorite()");
            $count = mysqli_fetch_row($resfavnum);
            //favorites will be added only if number is less than or equal to 7
            if($count[0]<7){
                //query to add favorite pokemon
                $conn = DBConnect();
                $resfav3 = $conn->query("call set_favorite(\"$id\",\"$name\")");
            }
        }else {
            //query to remove favorite pokemon
            $conn = DBConnect();
            $resfav2 = $conn->query("call del_favorite(\"$id\",\"$name\")");
        }
    }

    //query to get all pokemonn
    $conn = DBConnect();
    $result = $conn->query("CALL get_pokemon()");

        if($result){
            //load pokemons into a table
            $table = $result->fetch_all(MYSQLI_ASSOC);

            //query to get and load popular pokemons into a table
            $conn = DBConnect();
            $result2 = $conn->query("CALL get_populars()");
            $table2 = $result2->fetch_all(MYSQLI_ASSOC);

            //get and load favorite pokemons into a table
            $conn = DBConnect();
            $result3 = $conn->query("CALL get_favorites()");
            //check if any favorite pokemon exists
            if ($result3) {
                $table3 = $result3->fetch_all(MYSQLI_ASSOC);
            }else{
                $table3 = array("name"=>"none");
            }

            //setup twig
            $template = $twig->load('frontpage.twig.html');

            //call render to replace values in template with ones specified in my array
            //Since the return value is a string, I can echo it.
            echo $template->render(array("pokemons"=>$table, "populars"=>$table2, "favorites"=>$table3));

            $conn->close(); //clean up connection
            //Probably autoclose on exit, but just to be safe.
        }else {
            //One benefit is that we can load a full error page
            $template = $twig->load("error.twig.html");
            echo $template->render(array("message"=>"SQL error; query failed"));
        }
?>
