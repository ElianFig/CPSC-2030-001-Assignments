/*
Event Handlers - Start
*/

//Function to load correct form (login or register)
$("#logRegSubmit").click(function(){
    $('*').css('cursor', 'wait');
    changeForms();
    $('*').css('cursor', 'default');
});

$("#loginSubmit").click(function(){
    $('*').css('cursor', 'wait');
    //do necessary login validation
    userExists();
    $('*').css('cursor', 'default');
});

$("#regSubmit").click(function(){
    $('*').css('cursor', 'wait');
    //do necessary login validation
    registerUser();
    $('*').css('cursor', 'default');
});

/*
Event Handlers - Start
*/


/*
Useful Functions - Start
4 non-trivial JQuery features
*/
//Function to switch between login to register form
function changeForms() {
    $(".loginForm").css("display", "none");
    $(".registerForm").css("display", "block");
    $("#rUserName").val($("#userName").val());
    $("#rPwd").val($("#pwd").val());
}

//Function to randomly load a different background image each time the page is refreshed
$(window).on('load', function(){
    //generates random picture name
    let strClass = "img";
    strClass += randomInt(1, 5);
    //remove classes
    $("#background").removeClass();
    //add appropriate classes
    $("#background").addClass("bg");
    $("#background").addClass(strClass);
});
//Funtion to return a random number given min and max
function randomInt(min,max) // min and max included
{
    return Math.floor(Math.random()*(max-min+1)+min);
}

/*
Useful Functions - End
*/


/*
AJAX Functions - Start
*/

//Verify if user exist
function userExists() {
    let user = $("#userName").val();
    let pwd = $("#pwd").val();
    if ((user == "") || (pwd == "") || !(user.indexOf("@")>0)) {
        return false;
    } else {
        $.ajax({
            url: "server.php",
            method: "POST",
            data: {
                userLogin: user,
                pwd: pwd
            },
            success: function(msg){
                messageIndex = JSON.parse(msg);
               if (messageIndex[0].response == "non-existent") {
                   alert("Username non-existent, please register!");
                   changeForms();
               } else if (messageIndex[0].response == "Denied") {
                   alert("Username/Password Invalid!");
               } else if (messageIndex[0].response == "login") {
                   window.location.replace("index.php");
               }
            }
        })
    }
}


//Register user
function registerUser() {
    let fname = $("#fname").val();
    let lname = $("#lname").val();
    let mname = $("#mname").val();
    let user = $("#rUserName").val();
    let pwd = $("#rPwd").val();
    if ((user == "") || (pwd == "") || (fname == "") || (lname == "") || !(user.indexOf("@")>0)) {
        return false;
    } else {
        $.ajax({
            url: "server.php",
            method: "POST",
            data: {
                fname: fname,
                lname: lname,
                mname: mname,
                user: user,
                pwd: pwd
            },
            success: function(msg){
                messageIndex = JSON.parse(msg);
               if (messageIndex[0].response == "Denied") {
                   alert("User already exists, please login!");
                   window.location.replace("index.php");
               } else if (messageIndex[0].response == "login") {
                   alert("User registered successfully!");
                   window.location.replace("index.php");
               }
            }
        })
    }
}

/*
AJAX Functions - End
*/
