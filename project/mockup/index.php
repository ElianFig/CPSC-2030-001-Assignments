<?php
    require_once 'sqlUseful.php';
    require_once 'session.php';
    require_once './vendor/autoload.php';  //include the twig library.

    //echo "<script>console.log( 'Before Session: " . $_SESSION['user'] . "' );</script>";
    if (isset($_SESSION['user'])) {
        echo "<script>console.log( 'User in session session: " . $_SESSION['user'] . "' );</script>";
    } else {
        sessionStart();
    }

    $loader = new Twig_Loader_Filesystem('./templates'); //set to load from the ./templates directory

    //Sometimes you have to manually delete the cache
    $twig = new Twig_Environment($loader);

    //Check which page is requesting the information
    if(isset($_GET["page"]) and ($_GET["page"] == "dashboard")){
        //if the dashboard gets a flower parameter, than it means it's for a flower deletion
        if (isset($_GET["flower"])) {
            $conn = DBConnect();
            $flowerID = mysqli_real_escape_string($conn, $_GET["flower"]); //prevent SQL injection
            $conn = DBConnect();
            $resDelFlower = $conn->query("CALL del_flower(\"$flowerID\")");
            if (mysqli_affected_rows($conn) > 0) {
                echo '<script>alert("Flower deleted successfully!");</script>';
                echo "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=index.php?page=dashboard'>";
            }
        } else { //Otherwise, it's simply to load the flowers
            //load flower info
            $conn = DBConnect();
            $result = $conn->query("CALL get_flowers()");
            if ($result) {
                $table = $result->fetch_all(MYSQLI_ASSOC);

                //setup twig
                $templateDash = $twig->load('dashboard.twig.html');

                //call render to replace values in template with ones specified in my array
                //Since the return value is a string, I can echo it.
                echo $templateDash->render(array("flowers"=>$table));
                //echo $template->render(array("flowers"=>$table, "populars"=>$table2, "favorites"=>$table3));

                $conn->close(); //clean up connection
                //Probably autoclose on exit, but just to be safe.
            }else {
                //One benefit is that we can load a full error page
                $templateDash = $twig->load("error.twig.html");
                echo $templateDash->render(array("message"=>"SQL error; No flowers found"));
            }
        }

    } elseif (isset($_GET["page"]) and ($_GET["page"] == "management")) {
        if (isset($_GET["flower"])) {
            $conn = DBConnect();
            $flower = mysqli_real_escape_string($conn, $_GET["flower"]); //prevent SQL injection
            $conn = DBConnect();
            $resFlowerByID = $conn->query("CALL flower_by_ID(\"$flower\")");
            if ($resFlowerByID) {
                $tblFlowerID = $resFlowerByID->fetch_all(MYSQLI_ASSOC);
            }
        } elseif (isset($_GET["update"])) {
            $plant_ID = $_POST["plant_ID"];
            $name = $_POST["name"];
            $scientific = $_POST["scientific"];
            $kingdom = $_POST["kingdom"];
            $plant_order = $_POST["plant_order"];
            $family = $_POST["family"];
            $subfamily = $_POST["subfamily"];
            $tribe = $_POST["tribe"];
            $supertribe = $_POST["supertribe"];
            $genus = $_POST["genus"];
            $info = htmlentities(cleanStr($_POST["info"]));
            $image_url = str_replace('\\', '\\\\', $_POST["image_url"]);
            $info_url = $_POST["info_url"];
            $conn = DBConnect();
            if ($plant_ID > 0) {
                $resUpdateFlower = $conn->query("CALL update_plant(\"$plant_ID\",\"$name\",\"$scientific\",\"$kingdom\",\"$plant_order\",\"$family\",\"$subfamily\",\"$tribe\",\"$supertribe\",\"$genus\",\"$info\",\"$image_url\",\"$info_url\")");
            } else {
                $resInsertFlower = $conn->query("CALL insert_plant(\"$plant_ID\",\"$name\",\"$scientific\",\"$kingdom\",\"$plant_order\",\"$family\",\"$subfamily\",\"$tribe\",\"$supertribe\",\"$genus\",\"$info\",\"$image_url\",\"$info_url\")");
            }

            if (mysqli_affected_rows($conn) > 0) {
                echo '<script>alert("Flower info updated successfully!");</script>';
                echo "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=index.php?page=dashboard'>";
            } else {
                //load a full error page
                $errorPage = $twig->load("error.twig.html");
                echo $errorPage->render(array("message"=>"SQL error; No records changed!"));
            }

        } else {
            $tblFlowerID = array("");
        }
        $templateMgm = $twig->load("management.twig.html");
        echo $templateMgm->render(array("flowers"=>$tblFlowerID));

    } elseif (isset($_GET["page"]) and ($_GET["page"] == "environment")) {
        $templateEnv = $twig->load("environment.twig.html");
        echo $templateEnv->render();
    } elseif (isset($_GET["page"]) and ($_GET["page"] == "about")) {
        $templateAbt = $twig->load("about.twig.html");
        echo $templateAbt->render();
    } elseif (isset($_GET["page"]) and ($_GET["page"] == "contact")) {
        $templateCon = $twig->load("contact.twig.html");
        echo $templateCon->render();
    }
    else {
        //if no parameter is passed, then loads the login page
        if (!isset($_SESSION['user'])) {
            $template = $twig->load("login.twig.html");
            echo $template->render();
        } else {
            //if user is already in session, redirects to home page
            $template = $twig->load("index.twig.html");
            echo $template->render();
            echo "<script>console.log( 'Session Started: " . $_SESSION['user']['fname'] . "' );</script>";
            echo "<script>console.log( 'Type: " . $_SESSION['user']['type'] . "' );</script>";
        }
    }
?>
