<?php

function sessionStart(){
    session_start(); //start the PHP_session function
}

function sessionKill(){
    session_unset();
    session_destroy();
    session_start();
}

?>
