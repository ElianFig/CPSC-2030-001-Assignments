<?php
    require_once 'sqlUseful.php';
    require_once 'session.php';

    if ((array_key_exists("userLogin", $_POST)) and (array_key_exists("pwd", $_POST))) {
        //login rules
        $user = $_POST["userLogin"];
        $pwd = $_POST["pwd"];
        $output = array();
        //login action
        $conn = DBConnect();
        $resUserLogin = $conn->query("CALL get_user(\"$user\",\"$pwd\")");
        //Checking if user exists
        if (mysqli_num_rows($resUserLogin)==0) {
            array_push($output,array("response"=>"non-existent"));
            echo json_encode($output);
        } else {
            $tblUser = mysqli_fetch_array($resUserLogin,MYSQLI_ASSOC);
            if (($user == $tblUser["email"]) && ($tblUser["inputPwd"] == $tblUser["password"])) {
                //login successfull
                session_start(); //start the PHP_session function
                $_SESSION['user'] = $tblUser;
                array_push($output,array("response"=>"login"));

                echo json_encode($output);
            } else {
                array_push($output,array("response"=>"Denied"));
                echo json_encode($output);
            }
        }
    }

    if (array_key_exists("fname", $_POST)) {
        $fname = $_POST["fname"];
        $lname = $_POST["lname"];
        $mname = $_POST["mname"];
        $userName = $_POST["user"];
        $pwd = $_POST["pwd"];
        $type = "user";
        $output = array();

        //Verify if user already exists
        $conn = DBConnect();
        $resUserExist = $conn->query("CALL get_user(\"$userName\",\"$pwd\")");
        if (mysqli_num_rows($resUserExist)==0) {
            //register user
            $conn = DBConnect();
            $resUserReg = $conn->query("CALL register_user(\"$fname\",\"$lname\",\"$mname\",\"$userName\",\"$pwd\",\"$type\")");
            if (mysqli_affected_rows($conn) > 0) {
                $conn = DBConnect();
                $resUserRegistered = $conn->query("CALL get_user(\"$userName\",\"$pwd\")");
                $tblUserRegistered = mysqli_fetch_array($resUserRegistered,MYSQLI_ASSOC);
                sessionStart(); //start the PHP_session function
                $_SESSION['user'] = $tblUserRegistered;
                array_push($output,array("response"=>"login"));
                echo json_encode($output);
            }
        } else {
            array_push($output,array("response"=>"Denied"));
            echo json_encode($output);
        }
    }

?>
