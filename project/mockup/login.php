<?php
    require_once 'sqlUseful.php';
    require_once 'session.php';
    require_once 'index.php';

    //Check action requested
    if (isset($_GET["action"]) and ($_GET["action"] == "logout")) {
        sessionKill();
        echo "<script>window.location.replace('index.php');</script>";
    } else {
        //if no parameter is passed, then loads the login page
        if ($_SESSION['user'] == "") {
            $template = $twig->load("login.twig.html");
            echo $template->render();
        } else {
            //if user is already in session, redirects to home page
            $template = $twig->load("index.twig.html");
            echo $template->render();
        }
    }



?>
