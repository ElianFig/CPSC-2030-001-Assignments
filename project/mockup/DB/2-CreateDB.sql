/*
CREATE DATABASE
*/
CREATE DATABASE nursery;

/*
SET DATABASE AS DEFAULT
*/
USE nursery;

/*
CREATE TABLES STATEMENTS
*/
CREATE TABLE plants (
    plant_ID INTEGER NOT NULL AUTO_INCREMENT,
    name CHAR(50) NOT NULL,
    scientific_name CHAR(80),
    kingdom CHAR(50),
    plant_order CHAR(50),
    family CHAR(50),
    subfamily CHAR(50),
    tribe CHAR(50),
    supertribe CHAR(50),
    genus CHAR(50),
    info TEXT,
    image_url CHAR(255),
    info_url CHAR(100),
    PRIMARY KEY (plant_ID)
);

CREATE TABLE clade (
    plant_ID INTEGER,
    clade CHAR(50),
    PRIMARY KEY (plant_ID, clade),
    FOREIGN KEY (plant_ID) REFERENCES plants(plant_ID)
);

CREATE TABLE user (
    fname CHAR(50) NOT NULL,
    lname CHAR(50) NOT NULL,
    mname CHAR(50),
    email CHAR(100) NOT NULL,
    password CHAR(100),
    type CHAR(5),
    PRIMARY KEY (email)
);

/* CREATE VIEW
I have separated the different forms of pokemon due to the primary key constraint on the table.
Since the original pokemon and all it's forms have the same national ID, I decided to separate it in order to be able to use national ID as a primary key.
That being said, I still need to count the different forms on my queries, and to do that, I've created a view 'pokemon_form_all':
Please execute the command below BEFORE executing any of the SELECT queries. Thanks

CREATE VIEW pokemon_form_all AS
    SELECT nat_ID, name, type, type1, type2, HP, ATK, DEF, SAT, SDF, SPD, BST, img_url, favorite
    FROM pokemon
    UNION
    SELECT nat_ID, name, type, type1, type2, HP, ATK, DEF, SAT, SDF, SPD, BST, img_url, favorite
    FROM poke_form;
*/

/*
CREATE DB USER
*/
CREATE USER IF NOT EXISTS 'CPSC2030'@'localhost' IDENTIFIED VIA mysql_native_password USING '*F29CF4A1DE8DE0164B8FC2F1BE128965912C97EF';
GRANT ALL PRIVILEGES ON *.* TO 'CPSC2030'@'localhost' REQUIRE NONE WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;

/*
CREATE PROCEDURES TO BE USED IN Assignment 5
*/
/*function to get all flowers */
DELIMITER //
CREATE PROCEDURE get_flowers()
BEGIN
	SELECT plant_ID, name, scientific_name, info, image_url
	FROM plants;
END //
DELIMITER ;

/*function to get pokemon with parameter
DELIMITER //
CREATE PROCEDURE pokemon_by_type
(IN str text)
BEGIN
SELECT nat_ID, name, type1, type2, img_url
FROM pokemon_form_all
WHERE type1 = str
OR type2 = str;
END //
DELIMITER ;
*/

/*function to get flower using its ID*/
DELIMITER //
CREATE PROCEDURE flower_by_ID
(IN str INT)
BEGIN
SELECT *
FROM plants
WHERE plant_ID = str;
END //
DELIMITER ;

/*function to delete flower using its ID*/
DELIMITER //
CREATE PROCEDURE del_flower
(IN str INT)
BEGIN
DELETE
FROM clade
WHERE plant_ID = str;
DELETE
FROM plants
WHERE plant_ID = str;
END //
DELIMITER ;

/*function to get pokemon type weakness and resistance
DELIMITER //
CREATE PROCEDURE pokemon_type_stat
(IN str INT)
BEGIN
SELECT DISTINCT t.type, t.strong, t.weak, t.resistant, t.vulnerable
FROM ((pokemon_form_all p
       INNER JOIN poke_type pt ON p.nat_ID = pt.poke_ID)
				  INNER JOIN type t ON pt.type_ID = t.type_ID)
WHERE p.nat_ID = str;
END //
DELIMITER ;
*/
/*function to get pokemon forms
DELIMITER //
CREATE PROCEDURE pokemon_form
(IN str INT)
BEGIN
SELECT pf.nat_ID, pf.name, pf.type, pf.img_url
FROM pokemon p
     INNER JOIN poke_form pf ON p.nat_ID = pf.nat_ID
WHERE p.nat_ID = str;
END //
DELIMITER ;
*/

/*function to update flower information*/
DELIMITER //
CREATE PROCEDURE update_plant
(IN strPlant_Id INT, IN strName text, IN strScientific text, IN strKingdom text, IN strPlant_Order text, IN strFamily text, IN strSubfamily text, IN strTribe text, IN strSupertribe text, IN strGenus text, IN strInfo text, IN strImage_url text, IN strInfo_Url text)
BEGIN
UPDATE plants
SET name = strName,
scientific_name = strScientific,
kingdom = strKingdom,
plant_order = strPlant_Order,
family = strFamily,
subfamily = strSubfamily,
tribe = strTribe,
supertribe = strSupertribe,
genus = strGenus,
info = strInfo,
image_url = strImage_url,
info_url = strInfo_Url
WHERE plant_ID = strPlant_Id;
END //
DELIMITER ;

/*function to insert a flower*/
DELIMITER //
CREATE PROCEDURE insert_plant
(IN strPlant_Id INT, IN strName text, IN strScientific text, IN strKingdom text, IN strPlant_Order text, IN strFamily text, IN strSubfamily text, IN strTribe text, IN strSupertribe text, IN strGenus text, IN strInfo text, IN strImage_url text, IN strInfo_Url text)
BEGIN
INSERT INTO plants (name, scientific_name, kingdom, plant_order, family, subfamily, tribe, supertribe, genus, info, image_url, info_url) VALUES (
strName,
strScientific,
strKingdom,
strPlant_Order,
strFamily,
strSubfamily,
strTribe,
strSupertribe,
strGenus,
strInfo,
strImage_url,
strInfo_Url
);
END //
DELIMITER ;


/*function to get user using the username*/
DELIMITER //
CREATE PROCEDURE get_user
(IN strUser text, IN strPwd text)
BEGIN
SELECT fname, lname, mname, email, password, type, PASSWORD(strPwd) as inputPwd
FROM user
WHERE email = strUser;
END //
DELIMITER ;

/*function to insert an user*/
DELIMITER //
CREATE PROCEDURE register_user
(IN strFname text, IN strLname text, IN strMname text, IN strEmail text, IN strPassword text, IN strType text)
BEGIN
INSERT INTO user (fname, lname, mname, email, password, type) VALUES (
strFname,
strLname,
strMname,
strEmail,
PASSWORD(strPassword),
strType
);
END //
DELIMITER ;

/*function to remove favorite pokemons
DELIMITER //
CREATE PROCEDURE del_favorite
(IN id INT, IN str text)
BEGIN
UPDATE pokemon
SET favorite = FALSE
WHERE nat_ID = id
AND name = str;
UPDATE poke_form
SET favorite = FALSE
WHERE nat_ID = id
AND name = str;
END //
DELIMITER ;
*/
/*function to get favorites pokemons
DELIMITER //
CREATE PROCEDURE get_favorites()
BEGIN
	SELECT nat_ID, name, type1, type2, img_url
	FROM pokemon_form_all
  WHERE favorite = TRUE;
END //
DELIMITER ;
*/
/*function to check if pokemon is favorite
DELIMITER //
CREATE PROCEDURE is_favorite
(IN id INT, IN str text)
BEGIN
	SELECT nat_ID, name, type1, type2, img_url
	FROM pokemon_form_all
  WHERE favorite = TRUE
  AND nat_ID = id
  AND name = str;
END //
DELIMITER ;
*/
/*function to get all popular pokemons
DELIMITER //
CREATE PROCEDURE get_populars()
BEGIN
	SELECT nat_ID, name, link
	FROM poke_pop;
END //
DELIMITER ;
*/
/*count how many favorite pokemons
DELIMITER //
CREATE PROCEDURE count_favorite()
BEGIN
	SELECT COUNT(nat_ID)
    FROM pokemon_form_all
    WHERE favorite = TRUE;
END //
DELIMITER ;
*/

/*
INSERT VALUES INTO TABLES
*/
INSERT INTO plants (name, scientific_name, kingdom, plant_order, family, subfamily, tribe, supertribe, genus, info, image_url, info_url) VALUES ('Tulip', 'Tulipa gesneriana', 'Plantae', 'Liliales', 'Liliaceae', 'Lilioideae', 'Lilieae', NULL,'Tulipa', 'Tulips (Tulipa) form a genus of spring-blooming perennial herbaceous bulbiferous geophytes (having bulbs as storage organs). The flowers are usually large, showy and brightly coloured, generally red, pink, yellow, or white (usually in warm colours). They often have a different coloured blotch at the base of the tepals (petals and sepals, collectively), internally.', 'resources\\1.jpg', 'https://en.wikipedia.org/wiki/Tulip');
INSERT INTO plants (name, scientific_name, kingdom, plant_order, family, subfamily, tribe, supertribe, genus, info, image_url, info_url) VALUES ('Rose', 'Rosa rubiginosa', 'Plantae', 'Rosales', 'Rosaceae', 'Rosoideae', 'Roseae', NULL,'Rosa', 'A rose is a woody perennial flowering plant of the genus Rosa, in the family Rosaceae, or the flower it bears. There are over three hundred species and thousands of cultivars. They form a group of plants that can be erect shrubs, climbing or trailing with stems that are often armed with sharp prickles. Flowers vary in size and shape and are usually large and showy, in colours ranging from white through yellows and reds. Most species are native to Asia, with smaller numbers native to Europe, North America, and northwestern Africa.', 'resources\\2.jpg', 'https://en.wikipedia.org/wiki/Rose');
INSERT INTO plants (name, scientific_name, kingdom, plant_order, family, subfamily, tribe, supertribe, genus, info, image_url, info_url) VALUES ('Daisy', 'Bellis perennis', 'Plantae', 'Asterales', 'Asteraceae', NULL,NULL,NULL,'Bellis', 'It is a herbaceous perennial plant with short creeping rhizomes and rosettes of small rounded or spoon-shaped leaves that are from 3/4 to 2 inches (approx. 2-5 cm) long and grow flat to the ground. The species habitually colonises lawns, and is difficult to eradicate by mowing - hence the term "lawn daisy". Wherever it appears it is often considered an invasive weed.', 'resources\\3.jpg', 'https://en.wikipedia.org/wiki/Bellis_perennis');
INSERT INTO plants (name, scientific_name, kingdom, plant_order, family, subfamily, tribe, supertribe, genus, info, image_url, info_url) VALUES ('Orchids', 'Orchidaceae', 'Plantae', 'Asparagales', 'Orchidaceae', NULL,NULL,NULL,'Orchis', 'Orchids are easily distinguished from other plants, as they share some very evident, shared derived characteristics, or synapomorphies. Among these are: bilateral symmetry of the flower (zygomorphism), many resupinate flowers, a nearly always highly modified petal (labellum), fused stamens and carpels, and extremely small seeds.', 'resources\\4.jpg', 'https://en.wikipedia.org/wiki/Orchidaceae');
INSERT INTO plants (name, scientific_name, kingdom, plant_order, family, subfamily, tribe, supertribe, genus, info, image_url, info_url) VALUES ('Sunflower', 'Helianthus', 'Plantae', 'Asterales', 'Asteraceae', 'Asteroideae', 'Heliantheae', 'Helianthodae', 'Helianthus', 'Sunflowers are usually tall annual or perennial plants that in some species can grow to a height of 300 cm (120 in) or more. They bear one or more wide, terminal capitula (flower heads), with bright yellow ray florets at the outside and yellow or maroon (also known as a brown/red) disc florets inside. Several ornamental cultivars of H. annuus have red-colored ray florets; all of them stem from a single original mutant. During growth, sunflowers tilt during the day to face the sun, but stop once they begin blooming. This tracking of the sun in young sunflower heads is called heliotropism. By the time they are mature, sunflowers generally face east. The rough and hairy stem is branched in the upper part in wild plants, but is usually unbranched in domesticated cultivars. The petiolate leaves are dentate and often sticky. The lower leaves are opposite, ovate, or often heart-shaped.', 'resources\\5.jpg', 'https://en.wikipedia.org/wiki/Helianthus');
INSERT INTO plants (name, scientific_name, kingdom, plant_order, family, subfamily, tribe, supertribe, genus, info, image_url, info_url) VALUES ('Buttercup', 'Ranunculus', 'Plantae', 'Ranunculales', 'Ranunculaceae', 'Ranunculoideae', 'Ranunculeae', NULL,'Ranunculus', 'Buttercups are mostly perennial, but occasionally annual or biennial, herbaceous, aquatic or terrestrial plants, often with leaves in a rosette at the base of the stem. In many perennial species runners are sent out that will develop new plants with roots and rosettes at the distanced nodes. The leaves lack stipules, have stems, are palmately veined, entire, more or less deeply incised, or compound, and leaflets or leaf segments may be very fine and linear in aquatic species.', 'resources\\6.jpg', 'https://en.wikipedia.org/wiki/Ranunculus');
INSERT INTO plants (name, scientific_name, kingdom, plant_order, family, subfamily, tribe, supertribe, genus, info, image_url, info_url) VALUES ('Poppy', 'Papaver', 'Plantae', NULL,'Papaveraceae', 'Papaveroideae', NULL,NULL,NULL,'Poppies are herbaceous annual, biennial or short-lived perennial plants. Some species are monocarpic, dying after flowering. Poppies can be over a metre tall with flowers up to 15 centimetres across. Flowers of species (not cultivars) have 4 to 6 petals, many stamens forming a conspicuous whorl in the center of the flower and an ovary of from 2 to many fused carpels. The petals are showy, may be of almost any color and some have markings. The petals are crumpled in the bud and as blooming finishes, the petals often lie flat before falling away. In the temperate zones, poppies bloom from spring into early summer.', 'resources\\7.jpg', 'https://en.wikipedia.org/wiki/Poppy');
INSERT INTO plants (name, scientific_name, kingdom, plant_order, family, subfamily, tribe, supertribe, genus, info, image_url, info_url) VALUES ('Sacred Lotus', 'Nelumbo nucifera', 'Plantae', 'Proteales', 'Nelumbonaceae', NULL,NULL,NULL,'Nelumbo', 'Nelumbo nucifera, also known as Indian lotus, sacred lotus, bean of India, Egyptian bean or simply lotus, is one of two extant species of aquatic plant in the family Nelumbonaceae. It is often colloquially called a water lily. Under favorable circumstances the seeds of this aquatic perennial may remain viable for many years, with the oldest recorded lotus germination being from that of seeds 1,300 years old recovered from a dry lakebed in northeastern China.', 'resources\\8.jpg', 'https://en.wikipedia.org/wiki/Nelumbo_nucifera');
INSERT INTO plants (name, scientific_name, kingdom, plant_order, family, subfamily, tribe, supertribe, genus, info, image_url, info_url) VALUES ('Carnation', 'Dianthus caryophyllus', 'Plantae', 'Caryophyllales', 'Caryophyllaceae', NULL,NULL,NULL,'Dianthus', 'Dianthus caryophyllus, the carnation or clove pink, is a species of Dianthus. It is probably native to the Mediterranean region but its exact range is unknown due to extensive cultivation for the last 2,000 years. It is an herbaceous perennial plant growing to 80 cm tall. The leaves are glaucous greyish green to blue-green, slender, up to 15 cm long. The flowers are produced singly or up to five together in a cyme; they are 3-5 cm diameter, and sweetly scented; the original natural flower colour is bright pinkish-purple, but cultivars of other colours, including red, white, yellow and green, have been developed', 'resources\\9.jpg', 'https://en.wikipedia.org/wiki/Dianthus_caryophyllus');

INSERT INTO clade (plant_ID, clade) VALUES (1, 'Angiosperms');
INSERT INTO clade (plant_ID, clade) VALUES (1, 'Monocots');
INSERT INTO clade (plant_ID, clade) VALUES (2, 'Angiosperms');
INSERT INTO clade (plant_ID, clade) VALUES (2, 'Eudicots');
INSERT INTO clade (plant_ID, clade) VALUES (2, 'Rosids');
INSERT INTO clade (plant_ID, clade) VALUES (3, 'Angiosperms');
INSERT INTO clade (plant_ID, clade) VALUES (3, 'Eudicots');
INSERT INTO clade (plant_ID, clade) VALUES (3, 'Asterids');
INSERT INTO clade (plant_ID, clade) VALUES (4, 'Angiosperms');
INSERT INTO clade (plant_ID, clade) VALUES (4, 'Monocots');
INSERT INTO clade (plant_ID, clade) VALUES (5, 'Angiosperms');
INSERT INTO clade (plant_ID, clade) VALUES (5, 'Eudicots');
INSERT INTO clade (plant_ID, clade) VALUES (5, 'Asterids');
INSERT INTO clade (plant_ID, clade) VALUES (6, 'Angiosperms');
INSERT INTO clade (plant_ID, clade) VALUES (6, 'Eudicots');
INSERT INTO clade (plant_ID, clade) VALUES (8, 'Angiosperms');
INSERT INTO clade (plant_ID, clade) VALUES (8, 'Eudicots');
INSERT INTO clade (plant_ID, clade) VALUES (9, 'Angiosperms');
INSERT INTO clade (plant_ID, clade) VALUES (9, 'Eudicots');

INSERT INTO user (fname, lname, mname, email, password, type) VALUES ('Elian', 'Figueiredo', NULL, 'elian_gaspar@hotmail.com', PASSWORD('password'), 'admin');
INSERT INTO user (fname, lname, mname, email, password, type) VALUES ('John', 'Doe', NULL, 'doe@hotmail.com', PASSWORD('123'), 'user');
